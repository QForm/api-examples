asm = NET.addAssembly(''); % enter a path to QFormApiNEt.dll file
qform = QFormAPI.QForm;
qform.start_or_attach(''); % enter a path to x64 QForm folder

arg1 = QFormAPI.FileName;
arg1.file = ''; % enter a path to the QForm project file
qform.project_open(arg1);

% Temperature [�C] on the surface
arg2 = QFormAPI.FieldAtMinMax;
arg2.object_type = QFormAPI.ObjectType.Workpiece;
arg2.object_id = 1;
arg2.mesh_index = 0;
arg2.field = 'T';
arg2.field_source = QFormAPI.FieldSource.MainSimulation;
arg2.source_object = -1;
arg2.source_operation = -1;
arg2.on_surface = true;
ret2 = qform.field_min_max(arg2);

min_t = ret2.min_value
max_t = ret2.max_value

% Temperature [�C] in the middle of the workpiece
arg3 = QFormAPI.FieldAtPoint;
arg3.object_type = QFormAPI.ObjectType.Workpiece;
arg3.object_id = 1;
arg3.mesh_index = 0;
arg3.field = 'T';
arg3.field_source = QFormAPI.FieldSource.MainSimulation;
arg3.source_object = -1;
arg3.source_operation = -1;
arg3.x = 0;
arg3.y = 0;
arg3.z = 0;
arg3.on_surface = false;
ret3 = qform.field_at_point(arg3);

mid_t = ret3.value

length(ret3.value)

a = [min_t max_t mid_t]

for i = 1:length(a)
    a(i)
end