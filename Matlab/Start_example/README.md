This script shows you how to export the temperature values to MATLAB.

Specify the paths in the script:

1) % enter a path to QFormApiNEt.dll file, e.g., **C:\QForm\10.x.x\x64\QFormApiNet.dll**
2) % enter a path to x64 QForm folder, e.g., **C:\QForm\10.x.x\x64**
3) % enter a path to the QForm project file, e.g., **D:\QForm_samples\Matlab_export.qform**

You must have a workpiece in an already calculated project to export the values.

`min_t = ret2.min_value` returns the minimum temperature value of the workpiece on its surface

`max_t = ret2.max_value` returns the maximum temperature value of the workpiece on its surface

`mid_t = ret3.value` returns the temperature value in the (0,0,0) point of the workpiece