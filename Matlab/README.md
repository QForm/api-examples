This script shows you how to connect to QForm using *MATLAB*.

First off, you need to specify the path to QFormApiNet.dll, located in the x64 QForm folder.

```matlab
asm = NET.addAssembly(''); % specify the path to QFormApiNEt.dll according to the QForm version
```

Let's start QForm by API:

```matlab
qform = QFormAPI.QForm;
qform.start_or_attach(''); % specify the path to x64 QForm folder according to the QForm version
```

You can find the *Start* example in the repository and start QForm by script.