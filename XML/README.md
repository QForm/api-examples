*XML* for QForm API is quite similar to the *S-expr*, but of course, you can work with XML files using libraries and editors.

The body of XML script is located between the `<qform_batch>` and `</qform_batch>` root tags. This script generates one operation and writes a "Hello" to the simulation message log. You can either add and then invoke these two functions one by one in App Wizard or run a *Start_sample.xml*. 

```xml
<qform_batch>

<operation_create index="1">
	<id>1</id>
	<name>Operation_1</name>
	<parent>0</parent>
	<creation_mode>CreateAsNewProcess</creation_mode>
</operation_create>

<print index="2">
	<msg>Hello!</msg>
	<type>info</type>
</print>

</qform_batch>
```

You can find the *Start_sample.xml* in the repository.
