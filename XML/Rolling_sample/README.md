This script generates a rolling operation in the *general forming* module with only one pass. The *data* folder contains two files: *Roll.qshape* and *Workpiece.qshape*.

The script works with relative paths to the geometry, that's why you can replace the files without any code changes.

But of course if you rename the files or data folder you will need to edit the script: 

```xml
<!-- Initial function -->
<geometry_load_single_object index="3">
	<file>data/Roll.qshape</file>
	<object_type>Tool</object_type>
	<object_id>1</object_id>
</geometry_load_single_object>

<!-- Edited function -->
<geometry_load_single_object index="3">
	<file>renamed_folder/renamed_roll.qshape</file>
	<object_type>Tool</object_type>
	<object_id>1</object_id>
</geometry_load_single_object>
```

Pay attention to the drive and lubricant setting functions. For example, this block creates a drive to the project data base:

```xml
<!-- Creating of a universal drive for a rolling process -->
<db_object_create index="18">
	<path>doc:drive/mill_36</path>
	<drive_type>Universal</drive_type>
</db_object_create>

<!-- Setting a rotation around axis 1 -->
<db_property_set index="19">
	<db_path>doc:drive/mill_36</db_path>
	<prop_path>rot1</prop_path>
	<value>true</value>
</db_property_set>

<!--  The velocity of the roll is 36 [rpm] -->
<db_property_set index="20">
	<db_path>doc:drive/mill_36</db_path>
	<prop_path>parts/23/value:1/value</prop_path>
	<value>36</value>
</db_property_set>

<!--  Saving the changes -->
<db_object_save index="21">
	<source_path>doc:drive/mill_36</source_path>
	<target_path>doc:drive/mill_36</target_path>
</db_object_save>
```

You can find the *Rolling_sample.xml* in the repository.
