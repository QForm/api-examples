### Tracking points

This script allows you to import tracking points from a file to the process or export them to the file.

The first step is to create a .txt file with coordinates and numerations of tracking points.

You can open the example_points.txt file to see the structure. 

The first line equaled to "0	0	0	1" means that the tracking point №1 has coordinates x = 0, y = 0 and z = 0

###  How to import tracking points to the process?

1) Open a QForm project with an already calculated operation
2) Jump to the *Applications* on the main menu tab and press *Run Application*. Select *Tracking_points.exe* file.
3) Specify the path to the .txt file
4) Press *Import tracking points form file* 

### How to export  tracking points from the process?

  1- 2 Steps are the same. You must have tracking points on the workpiece to export them.

3. Press *Export tracking points to file*.

4. Input the *Operation ID* - the number of the operation in the [ ] next to the operation name.

5.  Specify the path to the .txt file. For example *D:/tracking_points.txt*. 