﻿Imports QFormAPI
Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim x As New List(Of Double)
        Dim y As New List(Of Double)
        Dim z As New List(Of Double)
        Dim nomer As New List(Of Double)
        Try
            For i = 0 To IO.File.ReadAllLines(TextBox1.Text).Count - 1
                x.Add(Split(IO.File.ReadAllLines(TextBox1.Text)(i), vbTab)(0))
                y.Add(Split(IO.File.ReadAllLines(TextBox1.Text)(i), vbTab)(1))
                z.Add(Split(IO.File.ReadAllLines(TextBox1.Text)(i), vbTab)(2))
                nomer.Add(Split(IO.File.ReadAllLines(TextBox1.Text)(i), vbTab)(3))
            Next
        Catch ex As Exception
            Interaction.MsgBox(ex.Message)
            Exit Sub
        End Try

        Dim qform As New QForm
        qform.start_or_attach("")
        For i = 0 To x.Count - 1
            Dim arg1 As New QFormAPI.TrackingPointParams
            arg1.id = nomer(i)
            arg1.object_type = ObjectType.Workpiece
            arg1.object_id = 1
            arg1.point_x = x(i)
            arg1.point_y = y(i)
            arg1.point_z = z(i)
            arg1.on_surface = False
            Dim ret1 As QFormAPI.ItemId = qform.tracking_point_create(arg1)
        Next
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim qform As New QForm
        qform.start_or_attach("")
        Dim x As New List(Of Double)
        Dim y As New List(Of Double)
        Dim z As New List(Of Double)
        Dim nomer As New List(Of Double)
        Dim lines As New List(Of String)
        Dim ret2 As QFormAPI.ItemList = qform.tracking_points_get()
        Dim opera_id As Integer = InputBox("Input the operation id")
        For i = 0 To ret2.objects.Count - 1
            Try
                Dim arg1 As New QFormAPI.GlobalItemId
                arg1.id = ret2.objects(i)
                arg1.operation = opera_id
                Dim ret1 As QFormAPI.MeshPoint = qform.tracking_point_get(arg1)
                lines.Add(String.Join(vbTab, ret1.x, ret1.y, ret1.z, ret2.objects(i)))
            Catch ex As Exception
                Interaction.MsgBox("Can't get a tracking point - " & ex.Message)
            End Try
        Next
        IO.File.WriteAllLines(InputBox("Input a path of the export file"), lines)
    End Sub

End Class

