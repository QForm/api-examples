﻿Imports QFormAPI


Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Dim data_array As New List(Of String)

        Dim QForm As New QForm()
        QForm.start_or_attach("C:\QForm\10.1.100.43\x64")  'Change a path to x64 folder 

        Dim arg1 As New QFormAPI.FileName
        arg1.file = "D:/API_extrusion.qform" 'enter a path to qform.file
        QForm.project_open(arg1)

        ' Temperature [°C]
        Dim arg2 As New QFormAPI.FieldAtMinMax
        arg2.object_type = ObjectType.Workpiece
        arg2.object_id = 0
        arg2.mesh_index = 0
        arg2.field = "T"
        arg2.field_source = FieldSource.MainSimulation
        arg2.source_object = -1
        arg2.source_operation = -1
        arg2.on_surface = False
        Dim ret2 As QFormAPI.FieldMinMax = QForm.field_min_max(arg2)

        ' Effective Stress [MPa]
        Dim arg3 As New QFormAPI.FieldAtMinMax
        arg3.object_type = ObjectType.ExtrTool
        arg3.object_id = 0
        arg3.mesh_index = 0
        arg3.field = "stress_eff"
        arg3.field_source = FieldSource.MainSimulation
        arg3.source_object = -1
        arg3.source_operation = -1
        arg3.on_surface = False
        Dim ret3 As QFormAPI.FieldMinMax = QForm.field_min_max(arg3)

        ' Mean Stress [MPa]
        Dim arg4 As New QFormAPI.FieldAtMinMax
        arg4.object_type = ObjectType.ExtrTool
        arg4.object_id = 0
        arg4.mesh_index = 0
        arg4.field = "stress_mean"
        arg4.field_source = FieldSource.MainSimulation
        arg4.source_object = -1
        arg4.source_operation = -1
        arg4.on_surface = False
        Dim ret4 As QFormAPI.FieldMinMax = QForm.field_min_max(arg4)

        ' Elastic strain
        Dim arg5 As New QFormAPI.FieldAtMinMax
        arg5.object_type = ObjectType.ExtrTool
        arg5.object_id = 0
        arg5.mesh_index = 0
        arg5.field = "strain_elast"
        arg5.field_source = FieldSource.MainSimulation
        arg5.source_object = -1
        arg5.source_operation = -1
        arg5.on_surface = False
        Dim ret5 As QFormAPI.FieldMinMax = QForm.field_min_max(arg5)

        data_array.Add("WP Temperature min/max")
        data_array(data_array.Count - 1) &= vbTab & Math.Round(ret2.min_value, 5, MidpointRounding.AwayFromZero)
        data_array(data_array.Count - 1) &= vbTab & Math.Round(ret2.max_value, 5, MidpointRounding.AwayFromZero)

        data_array.Add("Tool Effective stress min/max")
        data_array(data_array.Count - 1) &= vbTab & Math.Round(ret3.min_value, 5, MidpointRounding.AwayFromZero)
        data_array(data_array.Count - 1) &= vbTab & Math.Round(ret3.max_value, 5, MidpointRounding.AwayFromZero)

        data_array.Add("Tool Mean stress min/max")
        data_array(data_array.Count - 1) &= vbTab & Math.Round(ret4.min_value, 5, MidpointRounding.AwayFromZero)
        data_array(data_array.Count - 1) &= vbTab & Math.Round(ret4.max_value, 5, MidpointRounding.AwayFromZero)

        data_array.Add("Tool Elastic strain min/max")
        data_array(data_array.Count - 1) &= vbTab & Math.Round(ret5.min_value, 5, MidpointRounding.AwayFromZero)
        data_array(data_array.Count - 1) &= vbTab & Math.Round(ret5.max_value, 5, MidpointRounding.AwayFromZero)

        IO.File.WriteAllLines(TextBox1.Text, data_array)



    End Sub
End Class
