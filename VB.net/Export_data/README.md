This script shows you how to export values to a .txt file using VB.net.

Specify the paths to the x64 QForm folder, the QForm project, and the QFormApiNet.dll.

```vb
Dim QForm As New QForm()
QForm.start_or_attach("C:\QForm\10.1.100.43\x64")  'Change a path to x64 folder 

Dim arg1 As New QFormAPI.FileName
arg1.file = "D:/API_extrusion.qform" 'enter a path to qform.file
QForm.project_open(arg1)
```

As we work with results, the simulation must be calculated. Also, the script connects to the API_extrusion.qform file, located in the repository, but you can either download the attached file or adjust the script with your project file.

The script collects the min/max values of the fields: temperature - of the workpiece,  effective stress, mean stress, elastic strain - of the tool. Please note that your project must contain an extrusion tool and the workpiece otherwise, you will get an error.

After launching the program, specify the exported file's location, e.g., *D:/data.txt*, to the textbox. Then, the script writes all these values to the .txt file.