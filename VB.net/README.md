Some scripts use additional packages for an interpreter. We work in Visual Studio, but of course, you can use the IDE you want.

This script shows you how to connect to QForm using *VB.net*.

First off, you need to add the reference to the project. Go to the Reference Manager and specify the path to QFormApiNet.dll, located in the x64 QForm folder.

Let's start QForm by API:

```vb
Imports QFormAPI 
...
Dim QForm As New QForm()
QForm.start_or_attach("C:\QForm\10.1.100.46\x64") 'specify the path according to the QForm version
```

You can find the *Start* example in the repository and start QForm by script.