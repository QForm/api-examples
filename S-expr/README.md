*S-expression* (s-expr) - is an adaptive language of QForm API. The main pros and cons of using s-expr are:

**+** You do not need an IDE, start your script just in AppWizard or notepad.

**+** It is easy to use. Simplify the initial data preparation without any skills in programming.

**-** This approach does not allow you to create loops, conditions, etc. Just execution of code line by line, function by function.

**-** You can not start QForm by .scm - file.

Go to the function tree of App Wizard and find an *operation_create* function.

Specify the name and add a print function according to the code below:

```
#operation_create function

(operation_create
	(id 1)
	(name "Start_operation")
	(parent 0)
	(creation_mode CreateAsNewProcess))


#print function

(print
	(msg "Hello!")
	(type info))
```

You can find the *Start_example.scm* in the repository.
