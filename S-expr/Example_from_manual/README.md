This script prepares all the initial data for the sample from the manual. There are relative paths to geometry for this task. So you do not really need to specify the full paths in the script. The only thing you need is to download the "*data*" folder from the repository and place the script next to it. This folder is the same as the "*C:\QForm\10.x.x\geometry\cover*".

Run QForm and go to the *Applications* on the main menu tab and press *Run Application*. Select *base_example.scm* file.

If you uncomment the last lines of the script and enter the path, QForm will save the project and start the simulation immediately after the initial data preparation:

```
(project_save_as
	(file "enter the path"))

(start_simulation)
```

You can find the *base_example.scm* in the repository.
