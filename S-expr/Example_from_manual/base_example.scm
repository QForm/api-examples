﻿(operation_create
	(id 1)
	(name "Operation_1")
	(parent 0)
	(creation_mode CreateAsNewProcess))

; Problem type
(property_set
	(object_type Operation)
	(object_id 1)
	(path "dim_type")
	(property_type Value)
	(value "axis"))

(geometry_load
	(file "data\\cover_op1.dxf"))

; Material
(property_set
	(object_type Workpiece)
	(object_id 0)
	(path "params/material_id")
	(property_type Value)
	(value "std/Steels/Carbon steels/C22 (1-0402)"))

; Temperature [°C]
(property_set
	(object_type Workpiece)
	(object_id 0)
	(path "params/temper")
	(property_type Value)
	(value "1200"))

; Drive
(property_set
	(object_type Tool)
	(object_id 1)
	(path "drive_id")
	(property_type Value)
	(value "std/Mechanical press/25MN"))

; Drive
(property_set
	(object_type Tool)
	(object_id 2)
	(path "drive_id")
	(property_type Value)
	(value "qform/114/+OZ"))

; Lubricant
(property_set
	(object_type Tool)
	(object_id 1)
	(path "params/lubricant_id")
	(property_type Value)
	(value "std/Hot forging/Steels/Graphite + water"))

; Lubricant
(property_set
	(object_type Tool)
	(object_id 2)
	(path "params/lubricant_id")
	(property_type Value)
	(value "std/Hot forging/Steels/Graphite + water"))

; Material
(property_set
	(object_type Tool)
	(object_id 1)
	(path "params/material_id")
	(property_type Value)
	(value "std/L6 HRC42"))

; Material
(property_set
	(object_type Tool)
	(object_id 2)
	(path "params/material_id")
	(property_type Value)
	(value "std/L6 HRC42"))

; Temperature [°C]
(property_set
	(object_type Tool)
	(object_id 1)
	(path "params/temper")
	(property_type Value)
	(value "150"))

; Temperature [°C]
(property_set
	(object_type Tool)
	(object_id 2)
	(path "params/temper")
	(property_type Value)
	(value "150"))

; Coupled deformation
(property_set
	(object_type Tool)
	(object_id 1)
	(path "params/plastic_task")
	(property_type Value)
	(value "true"))

; Coupled deformation
(property_set
	(object_type Tool)
	(object_id 2)
	(path "params/plastic_task")
	(property_type Value)
	(value "true"))

(stop_cond_create_distance
	(id 1)
	(distance 22)
	(object_1_type Tool)
	(object_1_id 1)
	(object_2_type Tool)
	(object_2_id 2))

(bound_cond_create
	(id 1)
	(type Fixing)
	(object_type Tool)
	(object_id 1))

(bound_cond_set_rect
	(id 1)
	(size1_x 130)
	(size1_z 105)
	(size2_x 10)
	(size2_z -95))

(bound_cond_create
	(id 2)
	(type Fixing)
	(object_type Tool)
	(object_id 2))

(bound_cond_set_rect
	(id 2)
	(size1_x 130)
	(size1_z -25)
	(size2_x 10)
	(size2_z 35))

(blow_parameter_set
	(blow 1)
	(param CoolingInAir)
	(stop_condition 1)
	(value 5))
	
(blow_parameter_set
	(blow 1)
	(param CoolingInTool)
	(stop_condition 1)
	(value 2))
	
(operation_create
	(id 2)
	(name "Operation_2")
	(parent 1)
	(parent_defined true))
	
(operation_copy_from_parent
	(id 2)
	(copy_bound_conds false)
	(copy_tools false)
	(inherit_results true))
	
; Problem type
(property_set
	(object_type Operation)
	(object_id 2)
	(path "dim_type")
	(property_type Value)
	(value "_3d"))
	
(geometry_load
	(file "data\\cover_op2.shl"))
	
; Drive
(property_set
	(object_type Tool)
	(object_id 1)
	(path "drive_id")
	(property_type Value)
	(value "std/Mechanical press/25MN"))

; Drive
(property_set
	(object_type Tool)
	(object_id 2)
	(path "drive_id")
	(property_type Value)
	(value "qform/114/+OZ"))

; Lubricant
(property_set
	(object_type Tool)
	(object_id 1)
	(path "params/lubricant_id")
	(property_type Value)
	(value "std/Hot forging/Steels/Graphite + water"))

; Lubricant
(property_set
	(object_type Tool)
	(object_id 2)
	(path "params/lubricant_id")
	(property_type Value)
	(value "std/Hot forging/Steels/Graphite + water"))

; Material
(property_set
	(object_type Tool)
	(object_id 1)
	(path "params/material_id")
	(property_type Value)
	(value "std/L6 HRC42"))

; Material
(property_set
	(object_type Tool)
	(object_id 2)
	(path "params/material_id")
	(property_type Value)
	(value "std/L6 HRC42"))

; Temperature [°C]
(property_set
	(object_type Tool)
	(object_id 1)
	(path "params/temper")
	(property_type Value)
	(value "150"))

; Temperature [°C]
(property_set
	(object_type Tool)
	(object_id 2)
	(path "params/temper")
	(property_type Value)
	(value "150"))

; Coupled deformation
(property_set
	(object_type Tool)
	(object_id 1)
	(path "params/plastic_task")
	(property_type Value)
	(value "true"))

; Coupled deformation
(property_set
	(object_type Tool)
	(object_id 2)
	(path "params/plastic_task")
	(property_type Value)
	(value "true"))
	
(stop_cond_create_distance
	(id 1)
	(distance 2)
	(object_1_type Tool)
	(object_1_id 1)
	(object_2_type Tool)
	(object_2_id 2))
	
(bound_cond_create
	(id 1)
	(type Fixing)
	(object_type Tool)
	(object_id 1))

(bound_cond_set_cylinder
	(id 1)
	(r 130)
	(inner_r 0)
	(h1 130)
	(h2 -115))

(bound_cond_create
	(id 2)
	(type Fixing)
	(object_type Tool)
	(object_id 2))

(bound_cond_set_cylinder
	(id 2)
	(r 130)
	(inner_r 0)
	(h1 -40)
	(h2 25))
	

;(project_save_as
	(file "enter_the_path"))

;(start_simulation)