This script shows you how to connect to QForm using *VBA*.

1. The very first step is to make sure that *QFormApiCom.exe* is already installed on your PC. 
2. Next, open an *Excel Macro-Enabled Workbook* and go to *Visual Basic*. 
3. Add the reference to the *QFormAPI.tlb*.
4. Create two modules and use the first one to declare the global variable:

```visual basic
Global qform As QFormAPI.qform
```

5. Use the second module to start QForm:

```visual basic
Sub qstart()
Set qform = New qform
        qform.Start ("C:\QForm\10.1.100.46\x64") 'specify the path according to the QForm version
End Sub
```

You can find the *Start* example in the repository and start QForm by script.