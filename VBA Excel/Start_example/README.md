Do not forget to enter the correct version of QForm. 

After you've run QForm by script, open any project. Next, uncomment the *file_name* procedure and press F5 - the process name will appear in the A1 cell.

```visual basic
Sub file_name()
Dim ret1 As QFormAPI.Filename
Set ret1 = qform.project_file_get()
[A1] = ret1.file
End Sub
```
