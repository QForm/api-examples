Some scripts use additional packages for an interpreter. We work in Visual Studio, but of course, you can use the IDE you want.

This script shows you how to connect to QForm using *C#*.

First off, you need to add the reference to the project. Go to the Reference Manager and specify the path to QFormApiNet.dll, located in the x64 QForm folder. 

You can also directly import the *QForm.cs* file to the project - *"C:\QForm\10.x.x\API\App\C#\QForm.cs"*.

```c#
using QFormAPI; 
...
const string QFormPath = @"C:\QForm\10.1.100.46\x64"; //specify the path according to the QForm version
```

Let's start QForm by API:

```c#
QForm qform = new QForm();
qform.start_or_attach(QFormPath);
```

You can find the *Start* example in the repository and start QForm by script.
