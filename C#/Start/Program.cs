﻿using System;
using QFormAPI;


namespace Start
{
    class Program
    {
        const string QFormPath = @"C:\QForm\10.1.100.43\x64"; // path to x64 QForm folder

        static void Main(string[] args)
        {
            try
            {
                QForm qform = new QForm();
                qform.start_or_attach(QFormPath);

                OperationParams arg1 = new OperationParams();
                arg1.id = 1;
                arg1.name = @"Operation_1";
                arg1.parent = 0;
                arg1.creation_mode = OperationCreationMode.CreateAsNewProcess;
                ItemId ret1 = qform.operation_create(arg1);

                OperationParams arg2 = new OperationParams();
                arg2.id = 2;
                arg2.name = @"Operation_2";
                arg2.parent = 0;
                arg2.creation_mode = OperationCreationMode.CreateAsNewProcess;
                ItemId ret2 = qform.operation_create(arg2);

                TraceMsg arg3 = new TraceMsg();
                arg3.msg = @"There are 2 operations in your project";
                arg3.type = MessageType.info;
                qform.print(arg3);


                qform.detach(); // or qform.close() to exit
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.Message);
            }
        }
    }
}
