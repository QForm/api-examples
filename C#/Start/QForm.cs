﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace QFormAPI
{
	[AttributeUsage(AttributeTargets.Method)]
	public class ApiFunction : Attribute
	{
	}

    public
    class QForm
    {
        Process qfcon;
        bool m_attached = false;
        BinaryReader m_R;
        BinaryWriter m_W;

		~QForm()
		{
			close();
		}

        public class CmdInfo
        {
            public string arg;
            public string res;
        }

        public bool started()
        {
            return qfcon != null;
        }

        bool m_lost = false;
        public bool connection_lost()
        {
            return m_lost;
        }

        public bool attached()
        {
            return m_attached;
        }
		static bool console_mode()
		{
			return (Process.GetCurrentProcess().MainWindowHandle == IntPtr.Zero);
		}

		public void start_or_attach(string qform_exe_dir, bool show_qform = true, bool show_console = false)
        {
            string pid = Environment.GetEnvironmentVariable("CTL_MASTER_PID");

            if (!string.IsNullOrEmpty(pid))
            {
                string op = Environment.GetEnvironmentVariable("CTL_SLAVE_OUT");
                string osem = Environment.GetEnvironmentVariable("CTL_SLAVE_OUT_SEM");
                string ip = Environment.GetEnvironmentVariable("CTL_SLAVE_IN");
                string isem = Environment.GetEnvironmentVariable("CTL_SLAVE_IN_SEM");
                string exedir = Environment.GetEnvironmentVariable("CTL_MASTER_DIR");
                attach(exedir, pid, op, osem, ip, isem);
            }
            else
            {
                start(qform_exe_dir, show_qform, show_console);
            }
        }

        public void close()
        {
            if (m_attached)
                detach();
            else
                stop();
        }

        bool io_init()
        {
            m_lost = false;
            var istm = qfcon.StandardInput.BaseStream;
            var ostm = qfcon.StandardOutput.BaseStream;
            m_R = new BinaryReader(ostm);
            m_W = new BinaryWriter(istm);
            return true;
        }

        public void attach(string qform_exe_dir, string qform_pid, string out_pipe, string out_sem, string in_pipe, string in_sem)
        {
            if (qfcon != null)
                throw new Exception("QForm is already started");

            m_attached = false;
            qfcon = new Process();

            qfcon.StartInfo.UseShellExecute = false;
            qfcon.StartInfo.Arguments = "-bscm -attach -noconsole";
            qfcon.StartInfo.RedirectStandardOutput = true;
            qfcon.StartInfo.RedirectStandardInput = true;
            qfcon.StartInfo.FileName = qform_exe_dir + @"\QFormCon.exe";
            qfcon.StartInfo.CreateNoWindow = true;
            qfcon.Start();
            if (io_init())
            {
                m_attached = true;
                return;
            }

            detach();
            throw new Exception("QForm startup error");
        }

        public void start(string qform_exe_dir, bool show_qform = true, bool show_console = false)
        {
            if (qfcon != null)
                throw new Exception("QForm is already started");

			if (console_mode())
				show_console = true;

            m_attached = false;

            qfcon = new Process();
            qfcon.StartInfo.UseShellExecute = false;
            qfcon.StartInfo.RedirectStandardOutput = true;
            qfcon.StartInfo.RedirectStandardInput = true;

            string exe = qform_exe_dir + @"\QFormCon.exe";

            qfcon.StartInfo.FileName = exe;

            string args = "-bscm";

            if (!show_qform)
                args += " -nowindow";

            if (!show_console)
                args += " -noconsole";

            qfcon.StartInfo.Arguments = args;

            qfcon.Start();
            if (io_init())
                return;

            detach();
            throw new Exception("QForm startup error");
        }

        const int MSG_EXIT = -1;
        const int MSG_DETACH = -2;

		void disconnect(int msg)
		{
			if (qfcon == null)
			{
				close_io_handles();
				return;
			}

			if (qfcon.HasExited)
			{
				close_io_handles();
				qfcon.Dispose();
				qfcon = null;
				return;
			}

			if (m_W != null)
			{
				try
				{
					m_W.Write(msg);
					m_W.Flush();
				}
				catch (Exception)
				{
				}
			}

			if (m_R != null)
				m_R.Dispose();

			if (m_W != null)
				m_W.Dispose();

			qfcon.Close();
			qfcon.Dispose();

			m_R = null;
			m_W = null;
			qfcon = null;
		}

		void
			close_io_handles()
		{
			if (m_R != null)
				m_R.Dispose();

			if (m_W != null)
				m_W.Dispose();
		}

		void close_lost()
        {
            m_lost = true;

            if (qfcon == null)
                return;

            if (m_R != null)
                m_R.Dispose();

            if (m_W != null)
                m_W.Dispose();

            qfcon.Close();
            qfcon.Dispose();

            m_R = null;
            m_W = null;
            qfcon = null;
        }

        public void stop()
        {
			disconnect(MSG_EXIT);
		}

		public void detach()
        {
            disconnect(MSG_DETACH);
		}

		public Object invoke(string cmd, Object arg, Type ret_type, CmdInfo nfo = null)
        {
            if (qfcon == null)
                throw new Exception("QForm is not started");

            BScm msg = new BScm();
            if (arg != null)
                msg.store(arg);
            msg.value_str_set(cmd);
            int msz = msg.byte_size();
			m_W.Write(msz);
            msg.write(m_W);
            m_W.Flush();

            BScm response = new BScm();
			try
			{
				response.read(m_R);
			}
			catch (Exception ex)
			{
                close_lost();
				string emsg = ex.Message;
				throw new Exception("QForm connection lost");
			}

			if (nfo != null)
            {
                StringBuilder sb = new StringBuilder();
                nfo.arg = msg.print(sb);
                nfo.res = response.print(sb);
            }

            string sr = response.value_str_get();
            if (sr != "ok")
                throw new Exception(sr);

            if (ret_type == null)
                return null;

            Object ret = Activator.CreateInstance(ret_type);
            response.load(ret);
            return ret;
        }
		public int[] ApiVer = new int[] { 10, 1, 100, 42 };
		[ApiFunction] public FieldId active_field_get() {
			return (FieldId)invoke("active_field_get", null, typeof(FieldId)); }

		[ApiFunction] public void active_field_reset() {
			invoke("active_field_reset", null, null); }

		[ApiFunction] public void active_field_set(FieldId arg) {
			invoke("active_field_set", arg, null); }

		[ApiFunction] public ItemId assembled_tool_create(AssembledTool arg) {
			return (ItemId)invoke("assembled_tool_create", arg, typeof(ItemId)); }

		[ApiFunction] public AssembledTool assembled_tool_get(ItemId arg) {
			return (AssembledTool)invoke("assembled_tool_get", arg, typeof(AssembledTool)); }

		[ApiFunction] public void assembled_tool_split(ItemId arg) {
			invoke("assembled_tool_split", arg, null); }

		[ApiFunction] public ItemList assembled_tools_get() {
			return (ItemList)invoke("assembled_tools_get", null, typeof(ItemList)); }

		[ApiFunction] public SimulationResult async_calculate_tools(ObjectList arg) {
			return (SimulationResult)invoke("async_calculate_tools", arg, typeof(SimulationResult)); }

		[ApiFunction] public SimulationResult async_calculate_tools_coupled() {
			return (SimulationResult)invoke("async_calculate_tools_coupled", null, typeof(SimulationResult)); }

		[ApiFunction] public void async_execute_subroutines(SubroutineCalculationMode arg) {
			invoke("async_execute_subroutines", arg, null); }

		[ApiFunction] public void async_execute_tracking(TrackingCalculationMode arg) {
			invoke("async_execute_tracking", arg, null); }

		[ApiFunction] public void async_start_simulation(SimulationParams arg) {
			invoke("async_start_simulation", arg, null); }

		[ApiFunction] public AsyncState async_state() {
			return (AsyncState)invoke("async_state", null, typeof(AsyncState)); }

		[ApiFunction] public void async_stop_simulation() {
			invoke("async_stop_simulation", null, null); }

		[ApiFunction] public AsyncEvent async_wait(AsyncWaitingParams arg) {
			return (AsyncEvent)invoke("async_wait", arg, typeof(AsyncEvent)); }

		[ApiFunction] public void axis_delete(ObjectAxisId arg) {
			invoke("axis_delete", arg, null); }

		[ApiFunction] public ObjectAxis axis_get(ObjectAxisId arg) {
			return (ObjectAxis)invoke("axis_get", arg, typeof(ObjectAxis)); }

		[ApiFunction] public void axis_inherit(ObjectAxisId arg) {
			invoke("axis_inherit", arg, null); }

		[ApiFunction] public void axis_set(ObjectAxisParams arg) {
			invoke("axis_set", arg, null); }

		[ApiFunction] public Count billet_count_get() {
			return (Count)invoke("billet_count_get", null, typeof(Count)); }

		[ApiFunction] public void billet_count_set(Count arg) {
			invoke("billet_count_set", arg, null); }

		[ApiFunction] public ItemId billet_get_current() {
			return (ItemId)invoke("billet_get_current", null, typeof(ItemId)); }

		[ApiFunction] public NullableRealValue billet_parameter_get(BilletParameter arg) {
			return (NullableRealValue)invoke("billet_parameter_get", arg, typeof(NullableRealValue)); }

		[ApiFunction] public void billet_parameter_set(BilletParameter arg) {
			invoke("billet_parameter_set", arg, null); }

		[ApiFunction] public void billet_set_current(ItemId arg) {
			invoke("billet_set_current", arg, null); }

		[ApiFunction] public Count blow_count_get() {
			return (Count)invoke("blow_count_get", null, typeof(Count)); }

		[ApiFunction] public void blow_count_set(Count arg) {
			invoke("blow_count_set", arg, null); }

		[ApiFunction] public ItemId blow_get_current() {
			return (ItemId)invoke("blow_get_current", null, typeof(ItemId)); }

		[ApiFunction] public NullableRealValue blow_parameter_get(BlowParameter arg) {
			return (NullableRealValue)invoke("blow_parameter_get", arg, typeof(NullableRealValue)); }

		[ApiFunction] public void blow_parameter_set(BlowParameter arg) {
			invoke("blow_parameter_set", arg, null); }

		[ApiFunction] public void blow_set_current(ItemId arg) {
			invoke("blow_set_current", arg, null); }

		[ApiFunction] public ItemId bound_cond_create(BoundCondParams arg) {
			return (ItemId)invoke("bound_cond_create", arg, typeof(ItemId)); }

		[ApiFunction] public void bound_cond_delete(ItemId arg) {
			invoke("bound_cond_delete", arg, null); }

		[ApiFunction] public void bound_cond_set_body(ShapeBody arg) {
			invoke("bound_cond_set_body", arg, null); }

		[ApiFunction] public void bound_cond_set_brick(ShapeBirck arg) {
			invoke("bound_cond_set_brick", arg, null); }

		[ApiFunction] public void bound_cond_set_circle(ShapeCircle arg) {
			invoke("bound_cond_set_circle", arg, null); }

		[ApiFunction] public void bound_cond_set_cone(ShapeCone arg) {
			invoke("bound_cond_set_cone", arg, null); }

		[ApiFunction] public void bound_cond_set_cylinder(ShapeCylinder arg) {
			invoke("bound_cond_set_cylinder", arg, null); }

		[ApiFunction] public void bound_cond_set_rect(ShapeRect arg) {
			invoke("bound_cond_set_rect", arg, null); }

		[ApiFunction] public void bound_cond_set_sphere(ShapeSphere arg) {
			invoke("bound_cond_set_sphere", arg, null); }

		[ApiFunction] public void bound_cond_set_surface_by_color(ShapeSurfaceByColor arg) {
			invoke("bound_cond_set_surface_by_color", arg, null); }

		[ApiFunction] public BoundCondType bound_cond_type(ItemId arg) {
			return (BoundCondType)invoke("bound_cond_type", arg, typeof(BoundCondType)); }

		[ApiFunction] public SimulationResult calculate_tools(ObjectList arg) {
			return (SimulationResult)invoke("calculate_tools", arg, typeof(SimulationResult)); }

		[ApiFunction] public SimulationResult calculate_tools_coupled() {
			return (SimulationResult)invoke("calculate_tools_coupled", null, typeof(SimulationResult)); }

		[ApiFunction] public Chart chart_get(ChartId arg) {
			return (Chart)invoke("chart_get", arg, typeof(Chart)); }

		[ApiFunction] public ChartValue chart_value_get(ChartValueId arg) {
			return (ChartValue)invoke("chart_value_get", arg, typeof(ChartValue)); }

		[ApiFunction] public ContactArea contact_area(ObjectId arg) {
			return (ContactArea)invoke("contact_area", arg, typeof(ContactArea)); }

		[ApiFunction] public Field contact_field(FieldContact arg) {
			return (Field)invoke("contact_field", arg, typeof(Field)); }

		[ApiFunction] public DbItem db_fetch_items(DbFetchParams arg) {
			return (DbItem)invoke("db_fetch_items", arg, typeof(DbItem)); }

		[ApiFunction] public void db_object_create(DbObjectCreationParams arg) {
			invoke("db_object_create", arg, null); }

		[ApiFunction] public void db_object_export(SrcTargetPath arg) {
			invoke("db_object_export", arg, null); }

		[ApiFunction] public void db_object_import(SrcTargetPath arg) {
			invoke("db_object_import", arg, null); }

		[ApiFunction] public void db_object_save(SrcTargetPath arg) {
			invoke("db_object_save", arg, null); }

		[ApiFunction] public void db_objects_copy_to_project_file() {
			invoke("db_objects_copy_to_project_file", null, null); }

		[ApiFunction] public void db_objects_save_all() {
			invoke("db_objects_save_all", null, null); }

		[ApiFunction] public PropertyValue db_property_get(DbProperty arg) {
			return (PropertyValue)invoke("db_property_get", arg, typeof(PropertyValue)); }

		[ApiFunction] public void db_property_set(DbProperty arg) {
			invoke("db_property_set", arg, null); }

		[ApiFunction] public DbPropertyTable db_property_table_get(DbProperty arg) {
			return (DbPropertyTable)invoke("db_property_table_get", arg, typeof(DbPropertyTable)); }

		[ApiFunction] public void db_property_table_set(DbPropertyTable arg) {
			invoke("db_property_table_set", arg, null); }

		[ApiFunction] public void do_batch(BatchParams arg) {
			invoke("do_batch", arg, null); }

		[ApiFunction] public ItemId domain_create(DomainParams arg) {
			return (ItemId)invoke("domain_create", arg, typeof(ItemId)); }

		[ApiFunction] public void domain_delete(ItemId arg) {
			invoke("domain_delete", arg, null); }

		[ApiFunction] public void domain_set_body(ShapeBody arg) {
			invoke("domain_set_body", arg, null); }

		[ApiFunction] public void domain_set_brick(ShapeBirck arg) {
			invoke("domain_set_brick", arg, null); }

		[ApiFunction] public void domain_set_circle(ShapeCircle arg) {
			invoke("domain_set_circle", arg, null); }

		[ApiFunction] public void domain_set_cone(ShapeCone arg) {
			invoke("domain_set_cone", arg, null); }

		[ApiFunction] public void domain_set_cylinder(ShapeCylinder arg) {
			invoke("domain_set_cylinder", arg, null); }

		[ApiFunction] public void domain_set_rect(ShapeRect arg) {
			invoke("domain_set_rect", arg, null); }

		[ApiFunction] public void domain_set_sphere(ShapeSphere arg) {
			invoke("domain_set_sphere", arg, null); }

		[ApiFunction] public void domain_set_surface_by_color(ShapeSurfaceByColor arg) {
			invoke("domain_set_surface_by_color", arg, null); }

		[ApiFunction] public DomainType domain_type(ItemId arg) {
			return (DomainType)invoke("domain_type", arg, typeof(DomainType)); }

		[ApiFunction] public Contours dxf_parse_contours(FileName arg) {
			return (Contours)invoke("dxf_parse_contours", arg, typeof(Contours)); }

		[ApiFunction] public SimulationResult execute_subroutines() {
			return (SimulationResult)invoke("execute_subroutines", null, typeof(SimulationResult)); }

		[ApiFunction] public SimulationResult execute_subroutines_advanced(SubroutineCalculationMode arg) {
			return (SimulationResult)invoke("execute_subroutines_advanced", arg, typeof(SimulationResult)); }

		[ApiFunction] public void execute_tracking() {
			invoke("execute_tracking", null, null); }

		[ApiFunction] public SimulationResult execute_tracking_advanced(TrackingCalculationMode arg) {
			return (SimulationResult)invoke("execute_tracking_advanced", arg, typeof(SimulationResult)); }

		[ApiFunction] public void export_field_isosurface(ExportFieldIsosurface arg) {
			invoke("export_field_isosurface", arg, null); }

		[ApiFunction] public void export_fields_at_tracking_points(ExportFieldsAtTrackingPoints arg) {
			invoke("export_fields_at_tracking_points", arg, null); }

		[ApiFunction] public void export_mesh(MeshExport arg) {
			invoke("export_mesh", arg, null); }

		[ApiFunction] public void export_screenshot(ExportImage arg) {
			invoke("export_screenshot", arg, null); }

		[ApiFunction] public void export_section_mesh(ExportSection arg) {
			invoke("export_section_mesh", arg, null); }

		[ApiFunction] public void export_video(ExportImage arg) {
			invoke("export_video", arg, null); }

		[ApiFunction] public RealValue field_at_mesh_point(FieldAtMeshPoint arg) {
			return (RealValue)invoke("field_at_mesh_point", arg, typeof(RealValue)); }

		[ApiFunction] public VectorValue field_at_mesh_point_vector(FieldAtMeshPoint arg) {
			return (VectorValue)invoke("field_at_mesh_point_vector", arg, typeof(VectorValue)); }

		[ApiFunction] public RealValue field_at_point(FieldAtPoint arg) {
			return (RealValue)invoke("field_at_point", arg, typeof(RealValue)); }

		[ApiFunction] public VectorValue field_at_point_vector(FieldAtPoint arg) {
			return (VectorValue)invoke("field_at_point_vector", arg, typeof(VectorValue)); }

		[ApiFunction] public RealValues field_at_tracking_line(FieldAtTrackingObject arg) {
			return (RealValues)invoke("field_at_tracking_line", arg, typeof(RealValues)); }

		[ApiFunction] public VectorValues field_at_tracking_line_vector(FieldAtTrackingObject arg) {
			return (VectorValues)invoke("field_at_tracking_line_vector", arg, typeof(VectorValues)); }

		[ApiFunction] public RealValue field_at_tracking_point(FieldAtTrackingObject arg) {
			return (RealValue)invoke("field_at_tracking_point", arg, typeof(RealValue)); }

		[ApiFunction] public VectorValue field_at_tracking_point_vector(FieldAtTrackingObject arg) {
			return (VectorValue)invoke("field_at_tracking_point_vector", arg, typeof(VectorValue)); }

		[ApiFunction] public Field field_get(FieldAtMesh arg) {
			return (Field)invoke("field_get", arg, typeof(Field)); }

		[ApiFunction] public VectorField field_get_vector(FieldAtMesh arg) {
			return (VectorField)invoke("field_get_vector", arg, typeof(VectorField)); }

		[ApiFunction] public IsosurfaceList field_isosurface(FieldIsosurface arg) {
			return (IsosurfaceList)invoke("field_isosurface", arg, typeof(IsosurfaceList)); }

		[ApiFunction] public FieldMinMax field_min_max(FieldAtMinMax arg) {
			return (FieldMinMax)invoke("field_min_max", arg, typeof(FieldMinMax)); }

		[ApiFunction] public FieldMode field_mode_get() {
			return (FieldMode)invoke("field_mode_get", null, typeof(FieldMode)); }

		[ApiFunction] public void field_mode_set(FieldMode arg) {
			invoke("field_mode_set", arg, null); }

		[ApiFunction] public FieldPalette field_palette_get() {
			return (FieldPalette)invoke("field_palette_get", null, typeof(FieldPalette)); }

		[ApiFunction] public void field_palette_set(FieldPalette arg) {
			invoke("field_palette_set", arg, null); }

		[ApiFunction] public FieldStat field_stat(FieldStatAtMesh arg) {
			return (FieldStat)invoke("field_stat", arg, typeof(FieldStat)); }

		[ApiFunction] public FieldStat field_stat_at_section(FieldStatAtSection arg) {
			return (FieldStat)invoke("field_stat_at_section", arg, typeof(FieldStat)); }

		[ApiFunction] public StringList file_dialog(FileDlg arg) {
			return (StringList)invoke("file_dialog", arg, typeof(StringList)); }

		[ApiFunction] public void geometry_create_brick(BrickObjectParams arg) {
			invoke("geometry_create_brick", arg, null); }

		[ApiFunction] public void geometry_create_rect(RectObjectParams arg) {
			invoke("geometry_create_rect", arg, null); }

		[ApiFunction] public void geometry_create_sphere(SphereObjectParams arg) {
			invoke("geometry_create_sphere", arg, null); }

		[ApiFunction] public void geometry_create_tube(TubeObjectParams arg) {
			invoke("geometry_create_tube", arg, null); }

		[ApiFunction] public ObjectList geometry_load(FileName arg) {
			return (ObjectList)invoke("geometry_load", arg, typeof(ObjectList)); }

		[ApiFunction] public void geometry_load_extruded_object(ExtrudedObject arg) {
			invoke("geometry_load_extruded_object", arg, null); }

		[ApiFunction] public void geometry_load_revolved_object(RevolvedObject arg) {
			invoke("geometry_load_revolved_object", arg, null); }

		[ApiFunction] public void geometry_load_single_object(FileObject arg) {
			invoke("geometry_load_single_object", arg, null); }

		[ApiFunction] public BoolValue is_windowless_mode() {
			return (BoolValue)invoke("is_windowless_mode", null, typeof(BoolValue)); }

		[ApiFunction] public QFormLang language_get() {
			return (QFormLang)invoke("language_get", null, typeof(QFormLang)); }

		[ApiFunction] public void language_set(QFormLang arg) {
			invoke("language_set", arg, null); }

		[ApiFunction] public void log_begin(LogParams arg) {
			invoke("log_begin", arg, null); }

		[ApiFunction] public void log_save(LogFile arg) {
			invoke("log_save", arg, null); }

		[ApiFunction] public MeshCubics mesh_cubics_get(MeshObjectId arg) {
			return (MeshCubics)invoke("mesh_cubics_get", arg, typeof(MeshCubics)); }

		[ApiFunction] public MeshCoords mesh_lap_points_get(MeshObjectId arg) {
			return (MeshCoords)invoke("mesh_lap_points_get", arg, typeof(MeshCoords)); }

		[ApiFunction] public MeshCoords mesh_nodes_get(MeshObjectId arg) {
			return (MeshCoords)invoke("mesh_nodes_get", arg, typeof(MeshCoords)); }

		[ApiFunction] public MeshPoint mesh_point_get(ObjectPoint arg) {
			return (MeshPoint)invoke("mesh_point_get", arg, typeof(MeshPoint)); }

		[ApiFunction] public MeshProperties mesh_properties_get(MeshObjectId arg) {
			return (MeshProperties)invoke("mesh_properties_get", arg, typeof(MeshProperties)); }

		[ApiFunction] public MeshQuadrangles mesh_quadrangles_get(MeshObjectId arg) {
			return (MeshQuadrangles)invoke("mesh_quadrangles_get", arg, typeof(MeshQuadrangles)); }

		[ApiFunction] public MeshTetrahedrons mesh_thetrahedrons_get(MeshObjectId arg) {
			return (MeshTetrahedrons)invoke("mesh_thetrahedrons_get", arg, typeof(MeshTetrahedrons)); }

		[ApiFunction] public MeshTriangles mesh_triangles_get(MeshObjectId arg) {
			return (MeshTriangles)invoke("mesh_triangles_get", arg, typeof(MeshTriangles)); }

		[ApiFunction] public PressedDialogButton msg_box(MsgBox arg) {
			return (PressedDialogButton)invoke("msg_box", arg, typeof(PressedDialogButton)); }

		[ApiFunction] public ItemList object_axes_get(ObjectId arg) {
			return (ItemList)invoke("object_axes_get", arg, typeof(ItemList)); }

		[ApiFunction] public ItemList object_bound_conds_get(ObjectId arg) {
			return (ItemList)invoke("object_bound_conds_get", arg, typeof(ItemList)); }

		[ApiFunction] public RealValue object_contact(ObjectContact arg) {
			return (RealValue)invoke("object_contact", arg, typeof(RealValue)); }

		[ApiFunction] public void object_delete(ObjectId arg) {
			invoke("object_delete", arg, null); }

		[ApiFunction] public BoolValue object_display_mode_get(DisplayMode arg) {
			return (BoolValue)invoke("object_display_mode_get", arg, typeof(BoolValue)); }

		[ApiFunction] public void object_display_mode_set(DisplayMode arg) {
			invoke("object_display_mode_set", arg, null); }

		[ApiFunction] public ItemList object_domains_get(ObjectId arg) {
			return (ItemList)invoke("object_domains_get", arg, typeof(ItemList)); }

		[ApiFunction] public BoolValue object_exists(ObjectId arg) {
			return (BoolValue)invoke("object_exists", arg, typeof(BoolValue)); }

		[ApiFunction] public ObjectId object_find_by_color(FindByColor arg) {
			return (ObjectId)invoke("object_find_by_color", arg, typeof(ObjectId)); }

		[ApiFunction] public ObjectId object_find_by_surface_point(FindByPoint arg) {
			return (ObjectId)invoke("object_find_by_surface_point", arg, typeof(ObjectId)); }

		[ApiFunction] public void object_move(ObjectMove arg) {
			invoke("object_move", arg, null); }

		[ApiFunction] public void object_rotate(ObjectRotate arg) {
			invoke("object_rotate", arg, null); }

		[ApiFunction] public void object_set_type_by_color(TypeSetByColor arg) {
			invoke("object_set_type_by_color", arg, null); }

		[ApiFunction] public void object_set_type_by_surface_point(TypeSetByPoint arg) {
			invoke("object_set_type_by_surface_point", arg, null); }

		[ApiFunction] public void object_type_set(ObjectConvert arg) {
			invoke("object_type_set", arg, null); }

		[ApiFunction] public void object_type_set_in_direction(ObjectsInDirection arg) {
			invoke("object_type_set_in_direction", arg, null); }

		[ApiFunction] public ObjectList objects_find_by_color(FindByColor arg) {
			return (ObjectList)invoke("objects_find_by_color", arg, typeof(ObjectList)); }

		[ApiFunction] public ObjectList objects_get_in_direction(PickDirection arg) {
			return (ObjectList)invoke("objects_get_in_direction", arg, typeof(ObjectList)); }

		[ApiFunction] public ItemList operation_chains_get(ItemId arg) {
			return (ItemList)invoke("operation_chains_get", arg, typeof(ItemList)); }

		[ApiFunction] public OperationChecks operation_check(OptionalItemId arg) {
			return (OperationChecks)invoke("operation_check", arg, typeof(OperationChecks)); }

		[ApiFunction] public ItemId operation_copy(OperationCopy arg) {
			return (ItemId)invoke("operation_copy", arg, typeof(ItemId)); }

		[ApiFunction] public void operation_copy_from_parent(OperationCopyFromParent arg) {
			invoke("operation_copy_from_parent", arg, null); }

		[ApiFunction] public ItemId operation_create(OperationParams arg) {
			return (ItemId)invoke("operation_create", arg, typeof(ItemId)); }

		[ApiFunction] public void operation_cut(ItemId arg) {
			invoke("operation_cut", arg, null); }

		[ApiFunction] public void operation_delete(ItemId arg) {
			invoke("operation_delete", arg, null); }

		[ApiFunction] public BoolValue operation_exists(ItemId arg) {
			return (BoolValue)invoke("operation_exists", arg, typeof(BoolValue)); }

		[ApiFunction] public Operation operation_get(ItemId arg) {
			return (Operation)invoke("operation_get", arg, typeof(Operation)); }

		[ApiFunction] public ItemId operation_get_by_uid(ItemId arg) {
			return (ItemId)invoke("operation_get_by_uid", arg, typeof(ItemId)); }

		[ApiFunction] public ItemId operation_get_current() {
			return (ItemId)invoke("operation_get_current", null, typeof(ItemId)); }

		[ApiFunction] public OperationGraph operation_graph_get(ItemId arg) {
			return (OperationGraph)invoke("operation_graph_get", arg, typeof(OperationGraph)); }

		[ApiFunction] public ItemId operation_insert(OperationInsert arg) {
			return (ItemId)invoke("operation_insert", arg, typeof(ItemId)); }

		[ApiFunction] public ItemId operation_process_get(ItemId arg) {
			return (ItemId)invoke("operation_process_get", arg, typeof(ItemId)); }

		[ApiFunction] public void operation_set_current(ItemId arg) {
			invoke("operation_set_current", arg, null); }

		[ApiFunction] public ItemId operation_set_next_in_chain() {
			return (ItemId)invoke("operation_set_next_in_chain", null, typeof(ItemId)); }

		[ApiFunction] public ItemId operation_set_prev_in_chain() {
			return (ItemId)invoke("operation_set_prev_in_chain", null, typeof(ItemId)); }

		[ApiFunction] public void operation_template_set(DbObjectPath arg) {
			invoke("operation_template_set", arg, null); }

		[ApiFunction] public Operation operation_tree() {
			return (Operation)invoke("operation_tree", null, typeof(Operation)); }

		[ApiFunction] public ItemId operation_uid(OptionalItemId arg) {
			return (ItemId)invoke("operation_uid", arg, typeof(ItemId)); }

		[ApiFunction] public void print(TraceMsg arg) {
			invoke("print", arg, null); }

		[ApiFunction] public ItemId process_chain_get_current() {
			return (ItemId)invoke("process_chain_get_current", null, typeof(ItemId)); }

		[ApiFunction] public ItemList process_chain_get_current_operations() {
			return (ItemList)invoke("process_chain_get_current_operations", null, typeof(ItemList)); }

		[ApiFunction] public ItemList process_chain_get_operations(ItemId arg) {
			return (ItemList)invoke("process_chain_get_operations", arg, typeof(ItemList)); }

		[ApiFunction] public void process_chain_set_current(ItemId arg) {
			invoke("process_chain_set_current", arg, null); }

		[ApiFunction] public ItemList processes_get() {
			return (ItemList)invoke("processes_get", null, typeof(ItemList)); }

		[ApiFunction] public BoolValue project_ask_save() {
			return (BoolValue)invoke("project_ask_save", null, typeof(BoolValue)); }

		[ApiFunction] public FileName project_file_get() {
			return (FileName)invoke("project_file_get", null, typeof(FileName)); }

		[ApiFunction] public void project_new() {
			invoke("project_new", null, null); }

		[ApiFunction] public void project_open(FileName arg) {
			invoke("project_open", arg, null); }

		[ApiFunction] public void project_open_as_copy(ProjectOpenAsCopy arg) {
			invoke("project_open_as_copy", arg, null); }

		[ApiFunction] public void project_save() {
			invoke("project_save", null, null); }

		[ApiFunction] public void project_save_as(FileName arg) {
			invoke("project_save_as", arg, null); }

		[ApiFunction] public void project_save_as_template(PathName arg) {
			invoke("project_save_as_template", arg, null); }

		[ApiFunction] public PropertyValue property_get(Property arg) {
			return (PropertyValue)invoke("property_get", arg, typeof(PropertyValue)); }

		[ApiFunction] public void property_set(Property arg) {
			invoke("property_set", arg, null); }

		[ApiFunction] public ProcessId qform_process_id() {
			return (ProcessId)invoke("qform_process_id", null, typeof(ProcessId)); }

		[ApiFunction] public Record record_get() {
			return (Record)invoke("record_get", null, typeof(Record)); }

		[ApiFunction] public Record record_get_last() {
			return (Record)invoke("record_get_last", null, typeof(Record)); }

		[ApiFunction] public void record_set(Record arg) {
			invoke("record_set", arg, null); }

		[ApiFunction] public void results_truncate(Record arg) {
			invoke("results_truncate", arg, null); }

		[ApiFunction] public SectionMeshList section_mesh_get(SectionMeshPlane arg) {
			return (SectionMeshList)invoke("section_mesh_get", arg, typeof(SectionMeshList)); }

		[ApiFunction] public SectionPlane section_plane_get() {
			return (SectionPlane)invoke("section_plane_get", null, typeof(SectionPlane)); }

		[ApiFunction] public void section_plane_reset() {
			invoke("section_plane_reset", null, null); }

		[ApiFunction] public void section_plane_set(SectionPlane arg) {
			invoke("section_plane_set", arg, null); }

		[ApiFunction] public void section_plane_set_3p(SectionPlane3P arg) {
			invoke("section_plane_set_3p", arg, null); }

		[ApiFunction] public void section_plane_set_pn(SectionPlanePN arg) {
			invoke("section_plane_set_pn", arg, null); }

		[ApiFunction] public ObjectId selected_object_get() {
			return (ObjectId)invoke("selected_object_get", null, typeof(ObjectId)); }

		[ApiFunction] public ItemId session_id() {
			return (ItemId)invoke("session_id", null, typeof(ItemId)); }

		[ApiFunction] public ObjectList simulation_objects_get() {
			return (ObjectList)invoke("simulation_objects_get", null, typeof(ObjectList)); }

		[ApiFunction] public MainSimulationResult start_simulation() {
			return (MainSimulationResult)invoke("start_simulation", null, typeof(MainSimulationResult)); }

		[ApiFunction] public MainSimulationResult start_simulation_advanced(SimulationParams arg) {
			return (MainSimulationResult)invoke("start_simulation_advanced", arg, typeof(MainSimulationResult)); }

		[ApiFunction] public SimulationState state_blow(OptionalGlobalItemId arg) {
			return (SimulationState)invoke("state_blow", arg, typeof(SimulationState)); }

		[ApiFunction] public ExtrusionState state_extrusion(SystemStateId arg) {
			return (ExtrusionState)invoke("state_extrusion", arg, typeof(ExtrusionState)); }

		[ApiFunction] public MeshState state_mesh(MeshStateId arg) {
			return (MeshState)invoke("state_mesh", arg, typeof(MeshState)); }

		[ApiFunction] public SimulationState state_operation(OptionalItemId arg) {
			return (SimulationState)invoke("state_operation", arg, typeof(SimulationState)); }

		[ApiFunction] public SimulationState state_process_chain(OptionalItemId arg) {
			return (SimulationState)invoke("state_process_chain", arg, typeof(SimulationState)); }

		[ApiFunction] public SystemState state_system(SystemStateId arg) {
			return (SystemState)invoke("state_system", arg, typeof(SystemState)); }

		[ApiFunction] public ToolState state_tool(ToolStateId arg) {
			return (ToolState)invoke("state_tool", arg, typeof(ToolState)); }

		[ApiFunction] public WorkpieceState state_workpiece(WorkpieceStateId arg) {
			return (WorkpieceState)invoke("state_workpiece", arg, typeof(WorkpieceState)); }

		[ApiFunction] public ItemId stop_cond_create_distance(StopCondDistance arg) {
			return (ItemId)invoke("stop_cond_create_distance", arg, typeof(ItemId)); }

		[ApiFunction] public ItemId stop_cond_create_final_pos(StopCondFinPos arg) {
			return (ItemId)invoke("stop_cond_create_final_pos", arg, typeof(ItemId)); }

		[ApiFunction] public ItemId stop_cond_create_max_load(StopCondMaxLoad arg) {
			return (ItemId)invoke("stop_cond_create_max_load", arg, typeof(ItemId)); }

		[ApiFunction] public ItemId stop_cond_create_rotation(StopCondRotation arg) {
			return (ItemId)invoke("stop_cond_create_rotation", arg, typeof(ItemId)); }

		[ApiFunction] public ItemId stop_cond_create_stroke(StopCondStroke arg) {
			return (ItemId)invoke("stop_cond_create_stroke", arg, typeof(ItemId)); }

		[ApiFunction] public ItemId stop_cond_create_time(StopCondTime arg) {
			return (ItemId)invoke("stop_cond_create_time", arg, typeof(ItemId)); }

		[ApiFunction] public void stop_cond_delete(ItemId arg) {
			invoke("stop_cond_delete", arg, null); }

		[ApiFunction] public StopCond stop_cond_get(ItemId arg) {
			return (StopCond)invoke("stop_cond_get", arg, typeof(StopCond)); }

		[ApiFunction] public ItemList stop_conds_get() {
			return (ItemList)invoke("stop_conds_get", null, typeof(ItemList)); }

		[ApiFunction] public ItemId subroutine_create(SubroutineCreate arg) {
			return (ItemId)invoke("subroutine_create", arg, typeof(ItemId)); }

		[ApiFunction] public void subroutine_delete(ItemId arg) {
			invoke("subroutine_delete", arg, null); }

		[ApiFunction] public Subroutine subroutine_get(ItemId arg) {
			return (Subroutine)invoke("subroutine_get", arg, typeof(Subroutine)); }

		[ApiFunction] public NullableRealValue subroutine_parameter_get(SubroutineParameter arg) {
			return (NullableRealValue)invoke("subroutine_parameter_get", arg, typeof(NullableRealValue)); }

		[ApiFunction] public void subroutine_parameter_set(SubroutineParameter arg) {
			invoke("subroutine_parameter_set", arg, null); }

		[ApiFunction] public ItemList subroutines_get() {
			return (ItemList)invoke("subroutines_get", null, typeof(ItemList)); }

		[ApiFunction] public ItemId sym_plane_create_by_close_point(SymPlaneByPoint arg) {
			return (ItemId)invoke("sym_plane_create_by_close_point", arg, typeof(ItemId)); }

		[ApiFunction] public ItemId sym_plane_create_by_face_color(SymPlaneByColor arg) {
			return (ItemId)invoke("sym_plane_create_by_face_color", arg, typeof(ItemId)); }

		[ApiFunction] public void sym_plane_delete(ItemId arg) {
			invoke("sym_plane_delete", arg, null); }

		[ApiFunction] public UnitVector sym_plane_get(ItemId arg) {
			return (UnitVector)invoke("sym_plane_get", arg, typeof(UnitVector)); }

		[ApiFunction] public ItemList sym_planes_get() {
			return (ItemList)invoke("sym_planes_get", null, typeof(ItemList)); }

		[ApiFunction] public UnitSystem system_of_units_get() {
			return (UnitSystem)invoke("system_of_units_get", null, typeof(UnitSystem)); }

		[ApiFunction] public void system_of_units_set(UnitSystem arg) {
			invoke("system_of_units_set", arg, null); }

		[ApiFunction] public ItemId tracking_contour_create(NewTrackingContour arg) {
			return (ItemId)invoke("tracking_contour_create", arg, typeof(ItemId)); }

		[ApiFunction] public ItemId tracking_contours_create(OptionalObjectItemId arg) {
			return (ItemId)invoke("tracking_contours_create", arg, typeof(ItemId)); }

		[ApiFunction] public ItemId tracking_group_create(TrackingGroup arg) {
			return (ItemId)invoke("tracking_group_create", arg, typeof(ItemId)); }

		[ApiFunction] public ItemId tracking_line_create(TrackingLineParams arg) {
			return (ItemId)invoke("tracking_line_create", arg, typeof(ItemId)); }

		[ApiFunction] public TrackingLine tracking_line_get(GlobalItemId arg) {
			return (TrackingLine)invoke("tracking_line_get", arg, typeof(TrackingLine)); }

		[ApiFunction] public ItemList tracking_lines_get() {
			return (ItemList)invoke("tracking_lines_get", null, typeof(ItemList)); }

		[ApiFunction] public GlobalItemList tracking_lines_get_for_chain() {
			return (GlobalItemList)invoke("tracking_lines_get_for_chain", null, typeof(GlobalItemList)); }

		[ApiFunction] public ItemId tracking_point_create(TrackingPointParams arg) {
			return (ItemId)invoke("tracking_point_create", arg, typeof(ItemId)); }

		[ApiFunction] public MeshPoint tracking_point_get(GlobalItemId arg) {
			return (MeshPoint)invoke("tracking_point_get", arg, typeof(MeshPoint)); }

		[ApiFunction] public ItemList tracking_points_get() {
			return (ItemList)invoke("tracking_points_get", null, typeof(ItemList)); }

		[ApiFunction] public GlobalItemList tracking_points_get_for_chain() {
			return (GlobalItemList)invoke("tracking_points_get_for_chain", null, typeof(GlobalItemList)); }

		[ApiFunction] public void view_back() {
			invoke("view_back", null, null); }

		[ApiFunction] public void view_bottom() {
			invoke("view_bottom", null, null); }

		[ApiFunction] public void view_front() {
			invoke("view_front", null, null); }

		[ApiFunction] public View view_get() {
			return (View)invoke("view_get", null, typeof(View)); }

		[ApiFunction] public void view_left() {
			invoke("view_left", null, null); }

		[ApiFunction] public void view_on_bottom_90() {
			invoke("view_on_bottom_90", null, null); }

		[ApiFunction] public void view_on_top_90() {
			invoke("view_on_top_90", null, null); }

		[ApiFunction] public BoolValue view_option_get(ViewOption arg) {
			return (BoolValue)invoke("view_option_get", arg, typeof(BoolValue)); }

		[ApiFunction] public void view_option_set(ViewOption arg) {
			invoke("view_option_set", arg, null); }

		[ApiFunction] public void view_right() {
			invoke("view_right", null, null); }

		[ApiFunction] public void view_set(View arg) {
			invoke("view_set", arg, null); }

		[ApiFunction] public void view_top() {
			invoke("view_top", null, null); }

		[ApiFunction] public PathName work_dir_get() {
			return (PathName)invoke("work_dir_get", null, typeof(PathName)); }

		[ApiFunction] public void work_dir_set(PathName arg) {
			invoke("work_dir_set", arg, null); }

		[ApiFunction] public void zoom_to_fit() {
			invoke("zoom_to_fit", null, null); }

    }

    public class BScm
    {
        const byte _Shift = 128;

        public enum BType
        {
            Nil = 1,
            True = 2,
            False = 3,
            Str = 10,
            Wcs = 11,
            Bool = 20,
            Int = 21,
            Double = 22,
            ArrayBool = 40,
            ArrayInt = 41,
            ArrayDouble = 42,

        }

		class Value
        {
            public virtual BType type() { return BType.Nil; }
            public virtual void write(BinaryWriter W) {}
            public virtual void read(BinaryReader R) { }
            public virtual string name() { return null; }
            public virtual object val() { return null; }
            public virtual int byte_size() { return 0; }
        }

		class ValueT<T> : Value
        {
            public T v;
            public override object val() { return v; }
            public override string ToString()
            {
                return v.ToString();
            }
        }

		class ArrayT<T> : Value
        {
            public List<T> v;
            public override object val() { return v; }
            protected int read_size(BinaryReader R)
            {
                int n = R.ReadInt32();
                v = new List<T>();
                v.Capacity = n;
                return n;
            }
            public override string ToString()
            {
                int max_vals = 10;
                if (v.Count < max_vals)
                    max_vals = v.Count;
                StringBuilder sb = new StringBuilder();
                sb.Append("[");
                for (int i = 0; i < max_vals; i++)
                {
                    if (i > 0)
                        sb.Append(" ");
                    sb.Append(v[i].ToString());
                }
                if (v.Count > max_vals)
                    sb.AppendFormat("... {0} items", v.Count);
                sb.Append("]");
                return sb.ToString();
            }
        }

		class ValueInt : ValueT<int>
        {
            public ValueInt(int w = 0) { v = w; }
            public override BType type() { return BType.Int; }
            public override void write(BinaryWriter W) { W.Write(v);  }
            public override void read(BinaryReader R) { v = R.ReadInt32();  }
            public override int byte_size() { return sizeof(int); }
        }

		class ValueReal : ValueT<double>
        {
            public ValueReal(double w = 0) { v = w; }
            public override BType type() { return BType.Double; }
            public override void write(BinaryWriter W) { W.Write(v); }
            public override void read(BinaryReader R) { v = R.ReadDouble(); }
            public override int byte_size() { return sizeof(double); }
        }

		class ValueString : ValueT<string>
        {
            public ValueString(string w = null) { v = w; }
            public override string name() { return v; }
            public override BType type() { return BType.Str; }
            public override void write(BinaryWriter W)
            {
                byte[] bs = System.Text.Encoding.UTF8.GetBytes(v);
                W.Write(bs.Length);
                W.Write(bs);
            }
            public override int byte_size() { return sizeof(int) + System.Text.Encoding.UTF8.GetByteCount(v); }
        }

		class ValueWcs : ValueString
        {
            public override void read(BinaryReader R)
            {
                int n = R.ReadInt32();
                byte[] bs = R.ReadBytes(n * 2);
                v = System.Text.Encoding.Unicode.GetString(bs);
            }
        }

		class ValueStr : ValueString
        {
            public override void read(BinaryReader R)
            {
                int n = R.ReadInt32();
                byte[] cs = R.ReadBytes(n);
                v = System.Text.Encoding.UTF8.GetString(cs);
            }
        }

		class ValueBool : ValueT<bool>
        {
            public ValueBool(bool V) { v = V;  }
            public override BType type() { return v ? BType.True : BType.False; }
            public override void write(BinaryWriter W) {}
            public override void read(BinaryReader R) {}
            public override int byte_size() { return 0; }
        }

		class ArrayBool : ArrayT<bool>
        {
            public ArrayBool(List<bool> w = null) { v = w; }
            public override BType type() { return BType.ArrayBool; }
            public override void write(BinaryWriter W)
            {
                W.Write(v.Count);
                foreach (var p in v)
                    W.Write(p);
            }
            public override void read(BinaryReader R)
            {
                int n = read_size(R);
                for (int i = 0; i < n; i++)
                    v.Add(R.ReadBoolean());
            }
            public override int byte_size() { return sizeof(int) + sizeof(bool)*v.Count; }
        }

		class ArrayInt : ArrayT<int>
        {
            public ArrayInt(List<int> w = null) { v = w; }
            public override BType type() { return BType.ArrayInt; }
            public override void write(BinaryWriter W)
            {
                W.Write(v.Count);
                foreach (var p in v)
                    W.Write(p);
            }
            public override void read(BinaryReader R)
            {
                int n = read_size(R);
                for (int i = 0; i < n; i++)
                    v.Add(R.ReadInt32());
            }
            public override int byte_size() { return sizeof(int) + sizeof(int) * v.Count; }
        }

		class ArrayReal : ArrayT<double>
        {
            public ArrayReal(List<double> w = null) { v = w; }
            public override BType type() { return BType.ArrayDouble; }
            public override void write(BinaryWriter W)
            {
                W.Write(v.Count);
                foreach (var p in v)
                    W.Write(p);
            }
            public override void read(BinaryReader R)
            {
                int n = read_size(R);
                for (int i = 0; i < n; i++)
                    v.Add(R.ReadDouble());
            }
            public override int byte_size() { return sizeof(int) + sizeof(double) * v.Count; }
        }

        Value value;
        List<BScm> childs;
        public void read(BinaryReader R)
        {
            byte b = R.ReadByte();
            string dbg;
            if (b > _Shift)
            {
                b -= _Shift;
                dbg = read_value(R, (BType)b);
                int n = R.ReadInt32();
                childs = new List<BScm>(n);
                for (int i = 0; i < n; i++)
                {
                    BScm c = new BScm();
                    childs.Add(c);
                    c.read(R);
                }
            }
            else
            {
                dbg = read_value(R, (BType)b);
            }
        }
        public void write(BinaryWriter W)
        {
            byte t;
            if (value == null)
                t = (byte)BType.Nil;
            else
                t = (byte)value.type();

            if (childs != null)
            {
                t += _Shift;
                W.Write(t);
                write_value(W);
                W.Write(childs.Count);
                foreach (var c in childs)
                    c.write(W);
            }
            else
            {
                W.Write(t);
                write_value(W);
            }
        }

        void write_value(BinaryWriter W)
        {
            if (value != null)
                value.write(W);
        }

        Value create_value(BType t)
        {
            switch (t)
            {
                case BType.Nil:          return null;
                case BType.True:         return new ValueBool(true);
                case BType.False:        return new ValueBool(false);
                case BType.Str:          return new ValueStr();
                case BType.Wcs:          return new ValueWcs();
                case BType.Int:          return new ValueInt();
                case BType.Double:       return new ValueReal();
                case BType.ArrayBool:    return new ArrayBool();
                case BType.ArrayInt:     return new ArrayInt();
                case BType.ArrayDouble:  return new ArrayReal();
            }
            throw new Exception("Bad object format");
        }

        string read_value(BinaryReader R, BType t)
        {
            value = create_value(t);
            if (value != null)
            {
                value.read(R);
#if DEBUG
                return value.ToString();
#endif
            }
            return null;
        }

        void
            add(System.Reflection.PropertyInfo nfo, Value v)
        {
            BScm key = new BScm();
            childs.Add(key);
            key.value = new ValueString(nfo.Name);
            key.childs = new List<BScm>();
            BScm val = new BScm();
            key.childs.Add(val);
            val.value = v;
        }

        BScm
            prop_add(string name)
        {
            BScm key = add();
            key.value = new ValueString(name);
            key.childs = new List<BScm>();
            return key;
        }

        BScm
            add()
        {
            BScm key = new BScm();
            childs.Add(key);
            return key;
        }

        public void
            store(Object obj)
        {
            Type t = obj.GetType();
            childs = new List<BScm>();
            foreach(var p in t.GetProperties())
            {
                object v = p.GetValue(obj);
                if (v == null)
					continue;
                if (v is bool b)
                    add(p, new ValueBool(b));
                else if (v is int i)
                    add(p, new ValueInt(i));
                else if (v is double d)
                    add(p, new ValueReal(d));
                else if (v is string s)
                    add(p, new ValueString(s));
                else if (v is List<int> ai)
                    add(p, new ArrayInt(ai));
                else if (v is List<bool> ab)
                    add(p, new ArrayBool(ab));
                else if (v is List<double> ad)
                    add(p, new ArrayReal(ad));
                else if (p.PropertyType.IsEnum)
                    add(p, new ValueString(v.ToString()));
                else if (v is List<string> ls)
                {
                    if (ls.Count > 0)
                    {
                        BScm key = prop_add(p.Name);
                        foreach (string ss in ls)
                        {
                            BScm val = key.add();
                            val.value_str_set(ss);
                        }
                    }
                }
                else if (v is System.Collections.IList lst)
                {
                    if (lst.Count > 0)
                    {
                        BScm key = prop_add(p.Name);
                        foreach (Object o in lst)
                        {
                            BScm val = key.add();
                            val.store(o);
                        }
                    }
                }
            }

            if (childs.Count == 0)
                childs = null;
        }

        public string
            value_str_get()
        {
            if (value == null)
                return null;
            return value.name();
        }

        public void
            value_str_set(string str)
        {
            value = new ValueString(str);
        }

        System.Reflection.PropertyInfo
            find(System.Reflection.PropertyInfo[] props, string name, ref int size)
        {
            for (int i = 0; i < size; i++)
            {
                if (props[i].Name == name)
                {
                    var p = props[i];
                    --size;
                    props[i] = props[size];
                    return p;
                }
            }
            return null;
        }

		object load_list(Type pt)
		{
			Type itm_type = pt.GetGenericArguments()[0];

			if (itm_type == typeof(string))
			{
				var lst = Activator.CreateInstance(pt) as List<string>;
				foreach (var cs in childs)
					lst.Add(cs.value_str_get());
				return lst;
			}

			if (!itm_type.IsValueType)
			{
				var lst = Activator.CreateInstance(pt) as System.Collections.IList;
				foreach (var cs in childs)
				{
					var cobj = Activator.CreateInstance(itm_type);
					lst.Add(cobj);
					cs.load(cobj);
				}
				return lst;
			}
			return null;
		}

		void
            load(Object obj, System.Reflection.PropertyInfo p)
        {
            if (childs == null)
                return;

            BScm bval = childs[0];
            Type pt = p.PropertyType;
            if (pt.IsEnum)
            {
                string ename = bval.value_str_get();
                foreach(var ev in pt.GetEnumValues())
                {
                    if (pt.GetEnumName(ev) == ename)
                    {
                        p.SetValue(obj, ev);
                        break;
                    }
                }
                return;
            }

            if (pt.IsGenericType && pt.GetGenericTypeDefinition() == typeof(List<>))
            {
				object lst = load_list(pt);
				if (lst != null)
				{ 
					p.SetValue(obj, lst);
					return;
				}
            }

			object bv = bval.value.val();
			if (pt.IsClass)
			{
				Type obj_t = obj.GetType();
				if (obj_t.Namespace == pt.Namespace)
				{
					object ov = Activator.CreateInstance(pt);
					load(ov);
					p.SetValue(obj, ov);
					return;
				}
			}
			p.SetValue(obj, bv);
        }

        public void
            load(Object obj)
        {
            Type t = obj.GetType();
            var props = t.GetProperties();
            int prop_count = props.Length;
            foreach(BScm c in childs)
            {
                string nam = c.value_str_get();
                if (nam == null)
                    continue;
                var p = find(props, nam, ref prop_count);
                if (p != null)
                    c.load(obj, p);
            }
        }

        public int
            byte_size()
        {
            int sz = 1;
            if (value != null)
                sz += value.byte_size();

            if (childs != null)
            {
                sz += sizeof(int);
                foreach (var ch in childs)
                    sz += ch.byte_size();
            }

            return sz;
        }

        public string
            print(StringBuilder sb)
        {
            sb.Clear();
            do_print(sb);

            return sb.ToString();
        }

        void
            do_print(StringBuilder sb)
        {
            if (value != null)
                sb.Append(value.ToString());
            else
                sb.Append("()");

            if (childs == null)
                return;

            foreach (var c in childs)
            {
                if (c.childs != null)
                {
                    sb.Append(" (");
                    c.do_print(sb);
                    sb.Append(")");
                }
                else
                {
                    sb.Append(" ");
                    c.do_print(sb);
                }
            }
        }
    }
	public class FieldId {
		public string field { get; set; }
		public FieldSource field_source { get; set; }
		public int field_type { get; set; }
		public int field_target { get; set; }
		public int source_object { get; set; }
		public int source_operation { get; set; }
		public double field_min { get; set; }
		public double field_max { get; set; }
		public int units { get; set; }
	}

	public class AssembledTool {
		public int id { get; set; }
		List<int> x_parts = new List<int> ();
		public List<int> parts { get { return x_parts; } set { x_parts = value; } }
	}

	public class ItemId {
		public int id { get; set; }
	}

	public class ItemList {
		List<int> x_objects = new List<int> ();
		public List<int> objects { get { return x_objects; } set { x_objects = value; } }
	}

	public class ObjectList {
		List<ObjectId> x_objects = new List<ObjectId> ();
		public List<ObjectId> objects { get { return x_objects; } set { x_objects = value; } }
	}

	public class ObjectId {
		public ObjectType type { get; set; }
		public int id { get; set; }
	}

	public class SimulationResult {
		public StatusCode status { get; set; }
	}

	public class SubroutineCalculationMode {
		public CalculationUnit calculation_mode { get; set; }
	}

	public class TrackingCalculationMode {
		public CalculationUnit calculation_mode { get; set; }
		public bool forward { get; set; }
		public bool backward { get; set; }
	}

	public class SimulationParams {
		public int start_from_record { get; set; }
		public bool remesh_tools { get; set; }
		public int stop_at_record { get; set; }
		public int max_records { get; set; }
		public double stop_at_process_time { get; set; }
		public double max_process_time { get; set; }
		public int max_calculation_time { get; set; }
		public CalculationMode calculation_mode { get; set; }
	}

	public class AsyncState {
		public bool working { get; set; }
		public SimulationStage simulation_stage { get; set; }
		public int stage_time { get; set; }
		public int stage_counter { get; set; }
		public double record { get; set; }
		public int operation { get; set; }
		public int blow { get; set; }
	}

	public class AsyncWaitingParams {
		public int timeout { get; set; }
		public bool with_simulation_stage_events { get; set; }
		public bool with_diagnostic_events { get; set; }
		public bool with_iteration_events { get; set; }
	}

	public class AsyncEvent {
		public bool working { get; set; }
		public AsyncEventType type { get; set; }
		public CalculationUnit finished_calculation_unit { get; set; }
		public SimulationStage simulation_stage { get; set; }
		public int stage_time { get; set; }
		public int stage_counter { get; set; }
		public double record { get; set; }
		public double progress { get; set; }
		public double process_time { get; set; }
		public string diagnostic_msg { get; set; }
		public MessageType diagnostic_msg_type { get; set; }
		public int diagnostic_msg_code { get; set; }
		public int iteration { get; set; }
		public double iteration_velocity_norm { get; set; }
		public double iteration_mean_stress_norm { get; set; }
		public int iteration_separated_nodes { get; set; }
		public int iteration_sticked_nodes { get; set; }
		public int operation { get; set; }
		public int blow { get; set; }
		public bool backward { get; set; }
		public string units { get; set; }
	}

	public class ObjectAxisId {
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public int axis { get; set; }
	}

	public class ObjectAxis {
		public bool defined { get; set; }
		public bool inherited { get; set; }
		public double point1_x { get; set; }
		public double point1_y { get; set; }
		public double point1_z { get; set; }
		public double point2_x { get; set; }
		public double point2_y { get; set; }
		public double point2_z { get; set; }
		public string units { get; set; }
	}

	public class ObjectAxisParams {
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public int axis { get; set; }
		public double point1_x { get; set; }
		public double point1_y { get; set; }
		public double point1_z { get; set; }
		public double point2_x { get; set; }
		public double point2_y { get; set; }
		public double point2_z { get; set; }
	}

	public class Count {
		public int count { get; set; }
	}

	public class BilletParameter {
		public int billet { get; set; }
		public BilletParam param { get; set; }
		public double value { get; set; }
	}

	public class NullableRealValue {
		public double value { get; set; }
		public bool is_null { get; set; }
		public string units { get; set; }
	}

	public class BlowParameter {
		public int blow { get; set; }
		public BlowParam param { get; set; }
		public int stop_condition { get; set; }
		public double value { get; set; }
	}

	public class BoundCondParams {
		public int id { get; set; }
		public BCond type { get; set; }
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
	}

	public class ShapeBody {
		public int id { get; set; }
		public ObjectType source_body_type { get; set; }
		public int source_body_id { get; set; }
	}

	public class ShapeBirck {
		public int id { get; set; }
		public double size1_x { get; set; }
		public double size1_y { get; set; }
		public double size1_z { get; set; }
		public double size2_x { get; set; }
		public double size2_y { get; set; }
		public double size2_z { get; set; }
	}

	public class ShapeCircle {
		public int id { get; set; }
		public double r { get; set; }
		public double inner_r { get; set; }
	}

	public class ShapeCone {
		public int id { get; set; }
		public double angle { get; set; }
		public double d { get; set; }
		public double h { get; set; }
	}

	public class ShapeCylinder {
		public int id { get; set; }
		public double r { get; set; }
		public double inner_r { get; set; }
		public double h1 { get; set; }
		public double h2 { get; set; }
	}

	public class ShapeRect {
		public int id { get; set; }
		public double size1_x { get; set; }
		public double size1_z { get; set; }
		public double size2_x { get; set; }
		public double size2_z { get; set; }
	}

	public class ShapeSphere {
		public int id { get; set; }
		public double r { get; set; }
		public double inner_r { get; set; }
	}

	public class ShapeSurfaceByColor {
		public int id { get; set; }
		public int color_R { get; set; }
		public int color_G { get; set; }
		public int color_B { get; set; }
	}

	public class BoundCondType {
		public BCond type { get; set; }
	}

	public class ChartId {
		public ObjectType arg_object_type { get; set; }
		public int arg_object_id { get; set; }
		public int arg_subobject { get; set; }
		public int arg_id { get; set; }
		public ObjectType func_object_type { get; set; }
		public int func_object_id { get; set; }
		public int func_subobject { get; set; }
		public int func_id { get; set; }
	}

	public class Chart {
		List<double> x_arg_value = new List<double> ();
		public List<double> arg_value { get { return x_arg_value; } set { x_arg_value = value; } }
		List<bool> x_arg_has_value = new List<bool> ();
		public List<bool> arg_has_value { get { return x_arg_has_value; } set { x_arg_has_value = value; } }
		List<double> x_func_value = new List<double> ();
		public List<double> func_value { get { return x_func_value; } set { x_func_value = value; } }
		List<bool> x_func_has_value = new List<bool> ();
		public List<bool> func_has_value { get { return x_func_has_value; } set { x_func_has_value = value; } }
		public string func_units { get; set; }
		public string arg_units { get; set; }
		public string arg_name { get; set; }
		public string func_name { get; set; }
	}

	public class ChartValueId {
		public ObjectType arg_object_type { get; set; }
		public int arg_object_id { get; set; }
		public int arg_subobject { get; set; }
		public int arg_id { get; set; }
		public ObjectType func_object_type { get; set; }
		public int func_object_id { get; set; }
		public int func_subobject { get; set; }
		public int func_id { get; set; }
		public double record { get; set; }
	}

	public class ChartValue {
		public double arg_value { get; set; }
		public double func_value { get; set; }
		public bool arg_has_value { get; set; }
		public bool func_has_value { get; set; }
		public double record { get; set; }
		public string func_units { get; set; }
		public string arg_units { get; set; }
		public string arg_name { get; set; }
		public string func_name { get; set; }
	}

	public class ContactArea {
		public double contact_area { get; set; }
		public double total_area { get; set; }
		public string units { get; set; }
	}

	public class FieldContact {
		public ObjectType type { get; set; }
		public int id { get; set; }
		public bool in_elements { get; set; }
	}

	public class Field {
		List<double> x_values = new List<double> ();
		public List<double> values { get { return x_values; } set { x_values = value; } }
		List<bool> x_has_data = new List<bool> ();
		public List<bool> has_data { get { return x_has_data; } set { x_has_data = value; } }
		public bool only_on_surface { get; set; }
		public bool in_elements { get; set; }
		public string units { get; set; }
	}

	public class DbFetchParams {
		public string db_name { get; set; }
		public DbStandart db_standart { get; set; }
	}

	public class DbItem {
		public string name { get; set; }
		public string db_path { get; set; }
		List<DbItem> x_childs = new List<DbItem> ();
		public List<DbItem> childs { get { return x_childs; } set { x_childs = value; } }
	}

	public class DbObjectCreationParams {
		public string path { get; set; }
		public DriveType drive_type { get; set; }
	}

	public class SrcTargetPath {
		public string source_path { get; set; }
		public string target_path { get; set; }
	}

	public class DbProperty {
		public string db_path { get; set; }
		public string prop_path { get; set; }
		public string value { get; set; }
	}

	public class PropertyValue {
		public PropertyType property_type { get; set; }
		public string value { get; set; }
		public string units { get; set; }
	}

	public class DbPropertyTable {
		public string db_path { get; set; }
		public string prop_path { get; set; }
		List<double> x_row_arg_values = new List<double> ();
		public List<double> row_arg_values { get { return x_row_arg_values; } set { x_row_arg_values = value; } }
		List<double> x_column_arg_values = new List<double> ();
		public List<double> column_arg_values { get { return x_column_arg_values; } set { x_column_arg_values = value; } }
		List<double> x_layer_arg_values = new List<double> ();
		public List<double> layer_arg_values { get { return x_layer_arg_values; } set { x_layer_arg_values = value; } }
		public DbTableArg row_arg { get; set; }
		public DbTableArg column_arg { get; set; }
		public DbTableArg layer_arg { get; set; }
		List<double> x_values = new List<double> ();
		public List<double> values { get { return x_values; } set { x_values = value; } }
		public string units { get; set; }
	}

	public class BatchParams {
		public string file { get; set; }
		public string log_file { get; set; }
		public LogFormat log_format { get; set; }
		public bool log_input { get; set; }
		public bool log_output { get; set; }
	}

	public class DomainParams {
		public int id { get; set; }
		public Domain type { get; set; }
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
	}

	public class DomainType {
		public Domain type { get; set; }
	}

	public class FileName {
		public string file { get; set; }
	}

	public class Contours {
		List<Contour> x_contours = new List<Contour> ();
		public List<Contour> contours { get { return x_contours; } set { x_contours = value; } }
	}

	public class Contour {
		public string name { get; set; }
		List<double> x_point_x = new List<double> ();
		public List<double> point_x { get { return x_point_x; } set { x_point_x = value; } }
		List<double> x_point_y = new List<double> ();
		public List<double> point_y { get { return x_point_y; } set { x_point_y = value; } }
		List<double> x_point_z = new List<double> ();
		public List<double> point_z { get { return x_point_z; } set { x_point_z = value; } }
		public bool closed { get; set; }
		public string units { get; set; }
	}

	public class ExportFieldIsosurface {
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public int mesh_index { get; set; }
		public string field { get; set; }
		public FieldSource field_source { get; set; }
		public int source_object { get; set; }
		public int source_operation { get; set; }
		public double field_value { get; set; }
		public string file { get; set; }
		public SectionFormat format { get; set; }
		public LengthUnit mesh_units { get; set; }
	}

	public class ExportFieldsAtTrackingPoints {
		public string file { get; set; }
		public TrackingFieldsFormat file_format { get; set; }
		public ValuesOnSheet values_on_sheet { get; set; }
		public bool with_workpiece_points { get; set; }
		public bool with_tool_points { get; set; }
	}

	public class MeshExport {
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public string file { get; set; }
		public MeshFormat format { get; set; }
		public int mesh_index { get; set; }
	}

	public class ExportImage {
		public string file { get; set; }
		public int width { get; set; }
		public int height { get; set; }
		public bool keep_aspect_ratio { get; set; }
		public bool display_operation_name { get; set; }
		public bool display_record_number { get; set; }
		public bool display_time { get; set; }
		public bool display_time_step { get; set; }
		public bool zoom_to_fit { get; set; }
		public bool display_legend { get; set; }
		public int legend_width { get; set; }
	}

	public class ExportSection {
		public double point_x { get; set; }
		public double point_y { get; set; }
		public double point_z { get; set; }
		public double normal_x { get; set; }
		public double normal_y { get; set; }
		public double normal_z { get; set; }
		public bool defined { get; set; }
		public string units { get; set; }
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public int mesh_index { get; set; }
		public bool u_vector_defined { get; set; }
		public double u_vector_x { get; set; }
		public double u_vector_y { get; set; }
		public double u_vector_z { get; set; }
		public string file { get; set; }
		public SectionFormat format { get; set; }
		public LengthUnit mesh_units { get; set; }
	}

	public class FieldAtMeshPoint {
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public int mesh_index { get; set; }
		public string field { get; set; }
		public FieldSource field_source { get; set; }
		public int source_object { get; set; }
		public int source_operation { get; set; }
		List<int> x_node = new List<int> ();
		public List<int> node { get { return x_node; } set { x_node = value; } }
		List<double> x_node_weight = new List<double> ();
		public List<double> node_weight { get { return x_node_weight; } set { x_node_weight = value; } }
		public bool on_surface { get; set; }
	}

	public class RealValue {
		public double value { get; set; }
		public string units { get; set; }
	}

	public class VectorValue {
		public double x { get; set; }
		public double y { get; set; }
		public double z { get; set; }
		public string units { get; set; }
	}

	public class FieldAtPoint {
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public int mesh_index { get; set; }
		public string field { get; set; }
		public FieldSource field_source { get; set; }
		public int source_object { get; set; }
		public int source_operation { get; set; }
		public double x { get; set; }
		public double y { get; set; }
		public double z { get; set; }
		public bool on_surface { get; set; }
	}

	public class FieldAtTrackingObject {
		public int object_id { get; set; }
		public int object_operation { get; set; }
		public int mesh_index { get; set; }
		public string field { get; set; }
		public FieldSource field_source { get; set; }
		public int source_object { get; set; }
		public int source_operation { get; set; }
	}

	public class RealValues {
		List<double> x_values = new List<double> ();
		public List<double> values { get { return x_values; } set { x_values = value; } }
		public string units { get; set; }
	}

	public class VectorValues {
		List<double> x_x = new List<double> ();
		public List<double> x { get { return x_x; } set { x_x = value; } }
		List<double> x_y = new List<double> ();
		public List<double> y { get { return x_y; } set { x_y = value; } }
		List<double> x_z = new List<double> ();
		public List<double> z { get { return x_z; } set { x_z = value; } }
		public string units { get; set; }
	}

	public class FieldAtMesh {
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public int mesh_index { get; set; }
		public string field { get; set; }
		public FieldSource field_source { get; set; }
		public int source_object { get; set; }
		public int source_operation { get; set; }
	}

	public class VectorField {
		List<double> x_x = new List<double> ();
		public List<double> x { get { return x_x; } set { x_x = value; } }
		List<double> x_y = new List<double> ();
		public List<double> y { get { return x_y; } set { x_y = value; } }
		List<double> x_z = new List<double> ();
		public List<double> z { get { return x_z; } set { x_z = value; } }
		List<bool> x_has_data = new List<bool> ();
		public List<bool> has_data { get { return x_has_data; } set { x_has_data = value; } }
		public bool only_on_surface { get; set; }
		public string units { get; set; }
	}

	public class FieldIsosurface {
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public int mesh_index { get; set; }
		public string field { get; set; }
		public FieldSource field_source { get; set; }
		public int source_object { get; set; }
		public int source_operation { get; set; }
		public double field_value { get; set; }
	}

	public class IsosurfaceList {
		List<Isosurface> x_isosurfaces = new List<Isosurface> ();
		public List<Isosurface> isosurfaces { get { return x_isosurfaces; } set { x_isosurfaces = value; } }
	}

	public class Isosurface {
		List<double> x_coord_x = new List<double> ();
		public List<double> coord_x { get { return x_coord_x; } set { x_coord_x = value; } }
		List<double> x_coord_y = new List<double> ();
		public List<double> coord_y { get { return x_coord_y; } set { x_coord_y = value; } }
		List<double> x_coord_z = new List<double> ();
		public List<double> coord_z { get { return x_coord_z; } set { x_coord_z = value; } }
		List<int> x_source_node_1 = new List<int> ();
		public List<int> source_node_1 { get { return x_source_node_1; } set { x_source_node_1 = value; } }
		List<int> x_source_node_2 = new List<int> ();
		public List<int> source_node_2 { get { return x_source_node_2; } set { x_source_node_2 = value; } }
		List<double> x_intersection_t = new List<double> ();
		public List<double> intersection_t { get { return x_intersection_t; } set { x_intersection_t = value; } }
		List<int> x_triangle_node_1 = new List<int> ();
		public List<int> triangle_node_1 { get { return x_triangle_node_1; } set { x_triangle_node_1 = value; } }
		List<int> x_triangle_node_2 = new List<int> ();
		public List<int> triangle_node_2 { get { return x_triangle_node_2; } set { x_triangle_node_2 = value; } }
		List<int> x_triangle_node_3 = new List<int> ();
		public List<int> triangle_node_3 { get { return x_triangle_node_3; } set { x_triangle_node_3 = value; } }
		public string units { get; set; }
	}

	public class FieldAtMinMax {
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public int mesh_index { get; set; }
		public string field { get; set; }
		public FieldSource field_source { get; set; }
		public int source_object { get; set; }
		public int source_operation { get; set; }
		public bool on_surface { get; set; }
	}

	public class FieldMinMax {
		public double min_value { get; set; }
		public double min_x { get; set; }
		public double min_y { get; set; }
		public double min_z { get; set; }
		public int min_node { get; set; }
		public double max_value { get; set; }
		public double max_x { get; set; }
		public double max_y { get; set; }
		public double max_z { get; set; }
		public int max_node { get; set; }
		public bool has_values { get; set; }
		public string units { get; set; }
	}

	public class FieldMode {
		public FillMode mode { get; set; }
	}

	public class FieldPalette {
		public Colormap palette { get; set; }
		public bool inverse { get; set; }
	}

	public class FieldStatAtMesh {
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public int mesh_index { get; set; }
		public string field { get; set; }
		public FieldSource field_source { get; set; }
		public int source_object { get; set; }
		public int source_operation { get; set; }
		public int interval_count { get; set; }
		public HistogramBy histogram_by { get; set; }
		public double percentile_1_level { get; set; }
		public double percentile_2_level { get; set; }
	}

	public class FieldStat {
		public double area { get; set; }
		public double volume { get; set; }
		public double min_value { get; set; }
		public double max_value { get; set; }
		public double mean_value { get; set; }
		public double standart_deviation { get; set; }
		public double median { get; set; }
		public double coefficient_of_skewness { get; set; }
		public double excess_kurtosis { get; set; }
		public double percentile_1 { get; set; }
		public double percentile_2 { get; set; }
		List<double> x_histogram_field = new List<double> ();
		public List<double> histogram_field { get { return x_histogram_field; } set { x_histogram_field = value; } }
		List<double> x_histogram_level = new List<double> ();
		public List<double> histogram_level { get { return x_histogram_level; } set { x_histogram_level = value; } }
		public string units { get; set; }
	}

	public class FieldStatAtSection {
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public int mesh_index { get; set; }
		public string field { get; set; }
		public FieldSource field_source { get; set; }
		public int source_object { get; set; }
		public int source_operation { get; set; }
		public double section_point_x { get; set; }
		public double section_point_y { get; set; }
		public double section_point_z { get; set; }
		public double section_normal_x { get; set; }
		public double section_normal_y { get; set; }
		public double section_normal_z { get; set; }
		public int interval_count { get; set; }
		public double percentile_1_level { get; set; }
		public double percentile_2_level { get; set; }
	}

	public class FileDlg {
		public bool open_mode { get; set; }
		public string file_name { get; set; }
		public string file_ext { get; set; }
		List<string> x_filters = new List<string> ();
		public List<string> filters { get { return x_filters; } set { x_filters = value; } }
		public int current_filter { get; set; }
		public string current_folder { get; set; }
		public bool allow_multiple_files { get; set; }
		public string settings_key { get; set; }
	}

	public class StringList {
		List<string> x_items = new List<string> ();
		public List<string> items { get { return x_items; } set { x_items = value; } }
	}

	public class BrickObjectParams {
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public double size_x { get; set; }
		public double size_y { get; set; }
		public double size_z { get; set; }
		public bool hexahedral_mesh { get; set; }
		public int elem_count_x { get; set; }
		public int elem_count_y { get; set; }
		public int elem_count_z { get; set; }
	}

	public class RectObjectParams {
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public double size_x { get; set; }
		public double size_z { get; set; }
	}

	public class SphereObjectParams {
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public double r { get; set; }
		public double inner_r { get; set; }
	}

	public class TubeObjectParams {
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public double r { get; set; }
		public double inner_r { get; set; }
		public double h { get; set; }
		public double sector_angle { get; set; }
		public double sector_start_angle { get; set; }
		public bool hexahedral_mesh { get; set; }
		public int elem_count_axial { get; set; }
		public int elem_count_radial { get; set; }
		public int elem_count_tangential { get; set; }
	}

	public class ExtrudedObject {
		public string file { get; set; }
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public double depth { get; set; }
	}

	public class RevolvedObject {
		public string file { get; set; }
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public double angle { get; set; }
		public bool use_x_axis { get; set; }
	}

	public class FileObject {
		public string file { get; set; }
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
	}

	public class BoolValue {
		public bool value { get; set; }
	}

	public class QFormLang {
		public Language language { get; set; }
	}

	public class LogParams {
		public bool log_input { get; set; }
		public bool log_output { get; set; }
	}

	public class LogFile {
		public string file { get; set; }
		public LogFormat log_format { get; set; }
	}

	public class MeshObjectId {
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public int mesh_index { get; set; }
	}

	public class MeshCubics {
		List<int> x_node_1 = new List<int> ();
		public List<int> node_1 { get { return x_node_1; } set { x_node_1 = value; } }
		List<int> x_node_2 = new List<int> ();
		public List<int> node_2 { get { return x_node_2; } set { x_node_2 = value; } }
		List<int> x_node_3 = new List<int> ();
		public List<int> node_3 { get { return x_node_3; } set { x_node_3 = value; } }
		List<int> x_node_4 = new List<int> ();
		public List<int> node_4 { get { return x_node_4; } set { x_node_4 = value; } }
		List<int> x_node_5 = new List<int> ();
		public List<int> node_5 { get { return x_node_5; } set { x_node_5 = value; } }
		List<int> x_node_6 = new List<int> ();
		public List<int> node_6 { get { return x_node_6; } set { x_node_6 = value; } }
		List<int> x_node_7 = new List<int> ();
		public List<int> node_7 { get { return x_node_7; } set { x_node_7 = value; } }
		List<int> x_node_8 = new List<int> ();
		public List<int> node_8 { get { return x_node_8; } set { x_node_8 = value; } }
	}

	public class MeshCoords {
		List<double> x_x = new List<double> ();
		public List<double> x { get { return x_x; } set { x_x = value; } }
		List<double> x_y = new List<double> ();
		public List<double> y { get { return x_y; } set { x_y = value; } }
		List<double> x_z = new List<double> ();
		public List<double> z { get { return x_z; } set { x_z = value; } }
		public string units { get; set; }
	}

	public class ObjectPoint {
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public int mesh_index { get; set; }
		public double point_x { get; set; }
		public double point_y { get; set; }
		public double point_z { get; set; }
		public bool on_surface { get; set; }
	}

	public class MeshPoint {
		public double x { get; set; }
		public double y { get; set; }
		public double z { get; set; }
		public int mesh_index { get; set; }
		public bool on_surface { get; set; }
		List<int> x_node = new List<int> ();
		public List<int> node { get { return x_node; } set { x_node = value; } }
		List<double> x_node_weight = new List<double> ();
		public List<double> node_weight { get { return x_node_weight; } set { x_node_weight = value; } }
		public string units { get; set; }
	}

	public class MeshProperties {
		public int dim { get; set; }
		public int element_count_volumetric { get; set; }
		public int element_count_surface { get; set; }
		public int node_count { get; set; }
		public int node_count_internal { get; set; }
		public int node_count_surface { get; set; }
		public int triangle_count { get; set; }
		public int triangle_quad_count { get; set; }
		public int quadrangle_count { get; set; }
		public int tetrahedron_count { get; set; }
		public int cubic_count { get; set; }
		public int face_count { get; set; }
		public int edge_count { get; set; }
	}

	public class MeshQuadrangles {
		List<int> x_node_1 = new List<int> ();
		public List<int> node_1 { get { return x_node_1; } set { x_node_1 = value; } }
		List<int> x_node_2 = new List<int> ();
		public List<int> node_2 { get { return x_node_2; } set { x_node_2 = value; } }
		List<int> x_node_3 = new List<int> ();
		public List<int> node_3 { get { return x_node_3; } set { x_node_3 = value; } }
		List<int> x_node_4 = new List<int> ();
		public List<int> node_4 { get { return x_node_4; } set { x_node_4 = value; } }
	}

	public class MeshTetrahedrons {
		List<int> x_node_1 = new List<int> ();
		public List<int> node_1 { get { return x_node_1; } set { x_node_1 = value; } }
		List<int> x_node_2 = new List<int> ();
		public List<int> node_2 { get { return x_node_2; } set { x_node_2 = value; } }
		List<int> x_node_3 = new List<int> ();
		public List<int> node_3 { get { return x_node_3; } set { x_node_3 = value; } }
		List<int> x_node_4 = new List<int> ();
		public List<int> node_4 { get { return x_node_4; } set { x_node_4 = value; } }
	}

	public class MeshTriangles {
		List<int> x_node_1 = new List<int> ();
		public List<int> node_1 { get { return x_node_1; } set { x_node_1 = value; } }
		List<int> x_node_2 = new List<int> ();
		public List<int> node_2 { get { return x_node_2; } set { x_node_2 = value; } }
		List<int> x_node_3 = new List<int> ();
		public List<int> node_3 { get { return x_node_3; } set { x_node_3 = value; } }
	}

	public class MsgBox {
		public string msg { get; set; }
		public bool button_ok { get; set; }
		public bool button_cancel { get; set; }
		public bool button_yes { get; set; }
		public bool button_no { get; set; }
		public bool button_retry { get; set; }
		public bool button_continue { get; set; }
		public bool button_close { get; set; }
		public bool button_save_as { get; set; }
	}

	public class PressedDialogButton {
		public DialogButton button { get; set; }
	}

	public class ObjectContact {
		public ObjectType moving_object_type { get; set; }
		public int moving_object_id { get; set; }
		public ObjectType target_object_type { get; set; }
		public int target_object_id { get; set; }
		public Direction direction { get; set; }
		public bool reverse_direction { get; set; }
	}

	public class DisplayMode {
		public ObjectType type { get; set; }
		public int id { get; set; }
		public DisplayModes mode { get; set; }
		public bool value { get; set; }
	}

	public class FindByColor {
		public int color_R { get; set; }
		public int color_G { get; set; }
		public int color_B { get; set; }
		public bool pick_by_body_color { get; set; }
		public bool pick_by_face_color { get; set; }
	}

	public class FindByPoint {
		public double point_x { get; set; }
		public double point_y { get; set; }
		public double point_z { get; set; }
	}

	public class ObjectMove {
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public double vector_x { get; set; }
		public double vector_y { get; set; }
		public double vector_z { get; set; }
	}

	public class ObjectRotate {
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public double point_x { get; set; }
		public double point_y { get; set; }
		public double point_z { get; set; }
		public double vector_x { get; set; }
		public double vector_y { get; set; }
		public double vector_z { get; set; }
		public double angle { get; set; }
	}

	public class TypeSetByColor {
		public ObjectType type { get; set; }
		public int id { get; set; }
		public int color_R { get; set; }
		public int color_G { get; set; }
		public int color_B { get; set; }
		public bool pick_by_body_color { get; set; }
		public bool pick_by_face_color { get; set; }
	}

	public class TypeSetByPoint {
		public ObjectType type { get; set; }
		public int id { get; set; }
		public double point_x { get; set; }
		public double point_y { get; set; }
		public double point_z { get; set; }
	}

	public class ObjectConvert {
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public ObjectType new_type { get; set; }
		public int new_id { get; set; }
	}

	public class ObjectsInDirection {
		public double vector_x { get; set; }
		public double vector_y { get; set; }
		public double vector_z { get; set; }
		public bool pick_specified_type { get; set; }
		public ObjectType pick_type { get; set; }
		List<ObjectId> x_objects = new List<ObjectId> ();
		public List<ObjectId> objects { get { return x_objects; } set { x_objects = value; } }
	}

	public class PickDirection {
		public double vector_x { get; set; }
		public double vector_y { get; set; }
		public double vector_z { get; set; }
		public bool pick_specified_type { get; set; }
		public ObjectType pick_type { get; set; }
	}

	public class OptionalItemId {
		public int id { get; set; }
	}

	public class OperationChecks {
		List<string> x_errors = new List<string> ();
		public List<string> errors { get { return x_errors; } set { x_errors = value; } }
		List<string> x_warnings = new List<string> ();
		public List<string> warnings { get { return x_warnings; } set { x_warnings = value; } }
	}

	public class OperationCopy {
		public int id { get; set; }
		public int source { get; set; }
		public string name { get; set; }
		public string process_name { get; set; }
		public bool make_copy_active { get; set; }
	}

	public class OperationCopyFromParent {
		public int id { get; set; }
		public bool copy_bound_conds { get; set; }
		public bool copy_tools { get; set; }
		public bool copy_workpiece { get; set; }
		public bool inherit_results { get; set; }
	}

	public class OperationParams {
		public int id { get; set; }
		public string name { get; set; }
		public int parent { get; set; }
		public OperationCreationMode creation_mode { get; set; }
	}

	public class Operation {
		public int id { get; set; }
		public OperationType type { get; set; }
		public string name { get; set; }
		public string dsc { get; set; }
		List<Operation> x_childs = new List<Operation> ();
		public List<Operation> childs { get { return x_childs; } set { x_childs = value; } }
	}

	public class OperationGraph {
		public bool parent_defined { get; set; }
		public int parent { get; set; }
		List<int> x_childs = new List<int> ();
		public List<int> childs { get { return x_childs; } set { x_childs = value; } }
	}

	public class OperationInsert {
		public int id { get; set; }
		public string name { get; set; }
		List<int> x_childs = new List<int> ();
		public List<int> childs { get { return x_childs; } set { x_childs = value; } }
	}

	public class DbObjectPath {
		public string path { get; set; }
	}

	public class TraceMsg {
		public string msg { get; set; }
		public MessageType type { get; set; }
	}

	public class ProjectOpenAsCopy {
		public string source_path { get; set; }
		public string target_path { get; set; }
		public bool copy_simulation_results { get; set; }
	}

	public class PathName {
		public string path { get; set; }
	}

	public class Property {
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public string path { get; set; }
		public PropertyType property_type { get; set; }
		public string value { get; set; }
	}

	public class ProcessId {
		public int pid { get; set; }
	}

	public class Record {
		public double record { get; set; }
	}

	public class SectionMeshPlane {
		public double point_x { get; set; }
		public double point_y { get; set; }
		public double point_z { get; set; }
		public double normal_x { get; set; }
		public double normal_y { get; set; }
		public double normal_z { get; set; }
		public bool defined { get; set; }
		public string units { get; set; }
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public int mesh_index { get; set; }
		public bool u_vector_defined { get; set; }
		public double u_vector_x { get; set; }
		public double u_vector_y { get; set; }
		public double u_vector_z { get; set; }
	}

	public class SectionMeshList {
		List<SectionMesh> x_meshes = new List<SectionMesh> ();
		public List<SectionMesh> meshes { get { return x_meshes; } set { x_meshes = value; } }
		public double area { get; set; }
		public string units { get; set; }
	}

	public class SectionMeshNodes {
		List<double> x_coord_x = new List<double> ();
		public List<double> coord_x { get { return x_coord_x; } set { x_coord_x = value; } }
		List<double> x_coord_y = new List<double> ();
		public List<double> coord_y { get { return x_coord_y; } set { x_coord_y = value; } }
		List<double> x_coord_z = new List<double> ();
		public List<double> coord_z { get { return x_coord_z; } set { x_coord_z = value; } }
		List<double> x_coord_u = new List<double> ();
		public List<double> coord_u { get { return x_coord_u; } set { x_coord_u = value; } }
		List<double> x_coord_v = new List<double> ();
		public List<double> coord_v { get { return x_coord_v; } set { x_coord_v = value; } }
		List<int> x_source_node_1 = new List<int> ();
		public List<int> source_node_1 { get { return x_source_node_1; } set { x_source_node_1 = value; } }
		List<int> x_source_node_2 = new List<int> ();
		public List<int> source_node_2 { get { return x_source_node_2; } set { x_source_node_2 = value; } }
		List<double> x_intersection_t = new List<double> ();
		public List<double> intersection_t { get { return x_intersection_t; } set { x_intersection_t = value; } }
		public double u_vector_x { get; set; }
		public double u_vector_y { get; set; }
		public double u_vector_z { get; set; }
		public double v_vector_x { get; set; }
		public double v_vector_y { get; set; }
		public double v_vector_z { get; set; }
		public double uv_origin_x { get; set; }
		public double uv_origin_y { get; set; }
		public double uv_origin_z { get; set; }
		public string units { get; set; }
	}

	public class SectionMeshTriangles {
		List<int> x_node_1 = new List<int> ();
		public List<int> node_1 { get { return x_node_1; } set { x_node_1 = value; } }
		List<int> x_node_2 = new List<int> ();
		public List<int> node_2 { get { return x_node_2; } set { x_node_2 = value; } }
		List<int> x_node_3 = new List<int> ();
		public List<int> node_3 { get { return x_node_3; } set { x_node_3 = value; } }
	}

	public class SectionMeshBound {
		List<int> x_nodes = new List<int> ();
		public List<int> nodes { get { return x_nodes; } set { x_nodes = value; } }
		List<bool> x_nodes_is_on_edge = new List<bool> ();
		public List<bool> nodes_is_on_edge { get { return x_nodes_is_on_edge; } set { x_nodes_is_on_edge = value; } }
		public double length { get; set; }
		public double area { get; set; }
		public string units { get; set; }
	}

	public class SectionMesh {
		public SectionMeshNodes nodes { get; set; }
		public SectionMeshTriangles triangles { get; set; }
		List<SectionMeshBound> x_bounds = new List<SectionMeshBound> ();
		public List<SectionMeshBound> bounds { get { return x_bounds; } set { x_bounds = value; } }
		public double area { get; set; }
		public string units { get; set; }
	}

	public class SectionPlane {
		public double point_x { get; set; }
		public double point_y { get; set; }
		public double point_z { get; set; }
		public double normal_x { get; set; }
		public double normal_y { get; set; }
		public double normal_z { get; set; }
		public bool defined { get; set; }
		public string units { get; set; }
	}

	public class SectionPlane3P {
		public double x1 { get; set; }
		public double y1 { get; set; }
		public double z1 { get; set; }
		public double x2 { get; set; }
		public double y2 { get; set; }
		public double z2 { get; set; }
		public double x3 { get; set; }
		public double y3 { get; set; }
		public double z3 { get; set; }
		public double offset { get; set; }
		public bool reverse_cut { get; set; }
	}

	public class SectionPlanePN {
		public double point_x { get; set; }
		public double point_y { get; set; }
		public double point_z { get; set; }
		public double normal_x { get; set; }
		public double normal_y { get; set; }
		public double normal_z { get; set; }
		public double offset { get; set; }
		public bool reverse_cut { get; set; }
	}

	public class MainSimulationResult {
		public StatusCode status { get; set; }
		public CalculationUnit finished_calculation_unit { get; set; }
	}

	public class OptionalGlobalItemId {
		public int id { get; set; }
		public int operation { get; set; }
	}

	public class SimulationState {
		public bool simulation_completed { get; set; }
		public int process_chain { get; set; }
		public int operation_index_in_chain { get; set; }
		public int operation { get; set; }
		public int blow { get; set; }
		public int blow_record_count { get; set; }
		public double blow_progress { get; set; }
		public string units { get; set; }
	}

	public class SystemStateId {
		public double record { get; set; }
	}

	public class ExtrusionState {
		public double force { get; set; }
		public double fill_time { get; set; }
		public double euler_min_z { get; set; }
		public string units { get; set; }
	}

	public class MeshStateId {
		public double record { get; set; }
		public int id { get; set; }
		public ObjectType type { get; set; }
		public int mesh_index { get; set; }
	}

	public class MeshState {
		public int volumetric_elements { get; set; }
		public int surface_elements { get; set; }
		public int internal_nodes { get; set; }
		public int surface_nodes { get; set; }
		public int total_nodes { get; set; }
		public int dim { get; set; }
		public double record { get; set; }
	}

	public class SystemState {
		public double record { get; set; }
		public double time { get; set; }
		public double process_time { get; set; }
		public double time_step { get; set; }
		public string units { get; set; }
		public int iteration_count { get; set; }
		public double calculation_time { get; set; }
		public double total_calculation_time { get; set; }
		public double progress { get; set; }
	}

	public class ToolStateId {
		public double record { get; set; }
		public int id { get; set; }
		public ObjectType type { get; set; }
	}

	public class ToolState {
		public double record { get; set; }
		public double load { get; set; }
		public double energy { get; set; }
		public double displacement { get; set; }
		public double power { get; set; }
		public double torque_1 { get; set; }
		public double torque_2 { get; set; }
		public double work { get; set; }
		public double velocity { get; set; }
		public string units { get; set; }
	}

	public class WorkpieceStateId {
		public double record { get; set; }
		public int object_id { get; set; }
	}

	public class WorkpieceState {
		public double record { get; set; }
		public double volume { get; set; }
		public string units { get; set; }
		public bool with_laps { get; set; }
	}

	public class StopCondDistance {
		public int id { get; set; }
		public double distance { get; set; }
		public ObjectType object_1_type { get; set; }
		public int object_1_id { get; set; }
		public ObjectType object_2_type { get; set; }
		public int object_2_id { get; set; }
	}

	public class StopCondFinPos {
		public int id { get; set; }
		public ObjectType tool_type { get; set; }
		public int tool_number { get; set; }
	}

	public class StopCondMaxLoad {
		public int id { get; set; }
		public ObjectType tool_type { get; set; }
		public int tool_number { get; set; }
		public double max_load { get; set; }
	}

	public class StopCondRotation {
		public int id { get; set; }
		public ObjectType tool_type { get; set; }
		public int tool_number { get; set; }
		public double angle { get; set; }
		public int axis { get; set; }
	}

	public class StopCondStroke {
		public int id { get; set; }
		public ObjectType tool_type { get; set; }
		public int tool_number { get; set; }
		public double stroke { get; set; }
	}

	public class StopCondTime {
		public int id { get; set; }
		public double time { get; set; }
	}

	public class StopCond {
		public StopCondType type { get; set; }
	}

	public class SubroutineCreate {
		public int id { get; set; }
		public SubRoutineType type { get; set; }
		public string file { get; set; }
		public bool solve_in_process { get; set; }
	}

	public class Subroutine {
		List<int> x_used_subroutines = new List<int> ();
		public List<int> used_subroutines { get { return x_used_subroutines; } set { x_used_subroutines = value; } }
		List<string> x_parameters = new List<string> ();
		public List<string> parameters { get { return x_parameters; } set { x_parameters = value; } }
		List<string> x_input_fields = new List<string> ();
		public List<string> input_fields { get { return x_input_fields; } set { x_input_fields = value; } }
		List<string> x_output_fields = new List<string> ();
		public List<string> output_fields { get { return x_output_fields; } set { x_output_fields = value; } }
		public SubRoutineType type { get; set; }
		public string file { get; set; }
		public bool solve_in_process { get; set; }
	}

	public class SubroutineParameter {
		public int id { get; set; }
		public string parameter { get; set; }
		public double value { get; set; }
	}

	public class SymPlaneByPoint {
		public int id { get; set; }
		public double point_x { get; set; }
		public double point_y { get; set; }
		public double point_z { get; set; }
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
	}

	public class SymPlaneByColor {
		public int id { get; set; }
		public int color_R { get; set; }
		public int color_G { get; set; }
		public int color_B { get; set; }
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
	}

	public class UnitVector {
		public double vector_x { get; set; }
		public double vector_y { get; set; }
		public double vector_z { get; set; }
		public double point_x { get; set; }
		public double point_y { get; set; }
		public double point_z { get; set; }
		public string units { get; set; }
	}

	public class UnitSystem {
		public SystemOfUnits system { get; set; }
	}

	public class NewTrackingContour {
		public int index { get; set; }
		public int tracking_contours_id { get; set; }
		List<double> x_point_x = new List<double> ();
		public List<double> point_x { get { return x_point_x; } set { x_point_x = value; } }
		List<double> x_point_y = new List<double> ();
		public List<double> point_y { get { return x_point_y; } set { x_point_y = value; } }
		List<double> x_point_z = new List<double> ();
		public List<double> point_z { get { return x_point_z; } set { x_point_z = value; } }
	}

	public class OptionalObjectItemId {
		public int id { get; set; }
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
	}

	public class TrackingGroup {
		public int id { get; set; }
		public string name { get; set; }
	}

	public class TrackingLineParams {
		public int id { get; set; }
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public double point1_x { get; set; }
		public double point1_y { get; set; }
		public double point1_z { get; set; }
		public double point2_x { get; set; }
		public double point2_y { get; set; }
		public double point2_z { get; set; }
		public bool on_surface { get; set; }
	}

	public class GlobalItemId {
		public int id { get; set; }
		public int operation { get; set; }
	}

	public class TrackingLine {
		List<MeshPoint> x_points = new List<MeshPoint> ();
		public List<MeshPoint> points { get { return x_points; } set { x_points = value; } }
	}

	public class GlobalItemList {
		List<GlobalItemId> x_objects = new List<GlobalItemId> ();
		public List<GlobalItemId> objects { get { return x_objects; } set { x_objects = value; } }
	}

	public class TrackingPointParams {
		public int id { get; set; }
		public ObjectType object_type { get; set; }
		public int object_id { get; set; }
		public double point_x { get; set; }
		public double point_y { get; set; }
		public double point_z { get; set; }
		public bool on_surface { get; set; }
		public int group { get; set; }
	}

	public class View {
		public double point_x { get; set; }
		public double point_y { get; set; }
		public double point_z { get; set; }
		public double vector_x { get; set; }
		public double vector_y { get; set; }
		public double vector_z { get; set; }
	}

	public class ViewOption {
		public ViewOptions option { get; set; }
		public bool value { get; set; }
	}

	public enum FieldSource {
		MainSimulation = 2,
		ToolSimulation = 4,
		ToolCoupledSimulation = 5,
		Subroutine = 6,
		PhaseTransformations = 11,
		Sprayer = 12,
		Hardness = 13,
	}

	public enum ObjectType {
		Nothing = 0,
		Workpiece = 100,
		Tool = 150,
		AssembledTool = 50,
		SymmetryPlane = 800,
		BoundaryCondition = 450,
		Domain = 650,
		StopCondition = 500,
		Subroutine = 900,
		Operation = 600,
		ImportedObject = 10,
		MainRoll = 201,
		RollMandrell = 202,
		AxialRoll = 203,
		GuideRoll = 204,
		RollPlate = 205,
		RollEdger = 231,
		PressureRoll = 232,
		ClippingSurface = 401,
		TurningSurface = 402,
		TracingPoint = 701,
		TracingLines = 702,
		TracingBox = 703,
		TracingSurfLines = 704,
		ExtrWorkpiece = 301,
		ExtrToolMesh = 302,
		ExtrTool = 303,
		ExtrDieholder = 304,
		ExtrMandrell = 305,
		ExtrDieplace = 306,
		ExtrBacker = 307,
		ExtrBolster = 308,
		ExtrSpreader = 309,
		ExtrSeparator = 310,
		ElectroPusher = 351,
		ElectroAnvil = 352,
		ElectroClamp = 353,
		CrossRollingTool = 851,
		CrossRollingMandrelTool = 852,
		CrossRollingLinearTool = 853,
		CrossRollingFreeRotatedTool = 854,
	}

	public enum StatusCode {
		Ok = 0,
		Canceled = 1,
		Failed = 2,
		Working = 3,
		Idle = 4,
	}

	public enum CalculationUnit {
		Nothing = 0,
		Chain = 1,
		Operation = 2,
		Blow = 4,
		Billet = 12,
		Pass = 20,
		Records = 32,
		ProcessTime = 64,
		CalculationTime = 128,
	}

	public enum CalculationMode {
		Chain = 0,
		Operation = 2,
		Blow = 4,
		Billet = 12,
		Pass = 20,
	}

	public enum SimulationStage {
		Idle = 0,
		Working = 1,
		Preliminary = 2,
		Cooling = 3,
		Thermal = 4,
		Forwarding = 5,
		Remeshing = 6,
		Plastic = 7,
		Cutting = 8,
		Meshing = 9,
		Tool = 10,
		Diffusion = 11,
		GravitationalPos = 12,
	}

	public enum AsyncEventType {
		Idle = 0,
		Done = 1,
		NextCalculationUnit = 2,
		NextRecord = 3,
		NextStage = 4,
		Iteration = 5,
		Timeout = 6,
		Error = 7,
		Message = 8,
		Canceled = 9,
	}

	public enum MessageType {
		info = 0,
		error = 101,
		warning = 119,
	}

	public enum BilletParam {
		BilletTemperature = 7,
		TemperatureTaper = 8,
		VelocityValue = 9,
		ProfileVelocity = 10,
		BilletLength = 11,
		BilletToBilletPause = 12,
		MaxStroke = 13,
	}

	public enum BlowParam {
		StopConditionValue = 1,
		EnergyShare = 2,
		CoolingInAir = 3,
		CoolingInTool = 4,
		Feed = 5,
		Rotation = 6,
		RollingToolMotion = 15,
		UpperToolRotationSpeed = 16,
		LowerToolRotationSpeed = 17,
		SideToolRotationSpeed = 18,
	}

	public enum BCond {
		Nil = 0,
		Load = 1,
		Velocity = 2,
		Temperature = 5,
		HeatFlow = 6,
		HeatFlow_s = 7,
		HeatFlow_v = 8,
		Fixing = 9,
		Normal = 10,
		Bearing = 11,
		Ring = 12,
		Fit = 14,
		Friction = 16,
		Env = 17,
		Fastener = 22,
		Pressure = 27,
		Manipulator = 28,
		Rotation = 29,
		Pusher = 31,
		Sprayer = 32,
		SprayerRectArray = 33,
		SprayerPolarArray = 34,
		SprayerDB = 38,
		SprayerRectArrayDB = 39,
		SprayerPolarArrayDB = 40,
		BodyContour = 35,
		ConstantTemperatureContact = 36,
		Inductor = 37,
	}

	public enum DbStandart {
		Default = -1,
		File = 0,
		DIN = 1,
		AISI = 2,
		GOST = 3,
		BS = 4,
		JIS = 5,
		GB = 6,
		ISO = 7,
	}

	public enum DriveType {
		Unspecified = 0,
		Fixed = 2,
		Free = 12,
		MechanicalPresse = 4,
		Hydraulic = 5,
		LoadHolder = 7,
		Universal = 6,
		Tabular = 11,
		ScrewPress = 9,
		Hammer = 8,
	}

	public enum PropertyType {
		Value = 1,
		DbObject = 2,
		Inherited = 4,
		Automatic = 5,
	}

	public enum DbTableArg {
		Nothing = -1,
		Strain = 18,
		StrainRate = 19,
		Temperature = 10,
		Time = 7,
		Pressure = 38,
		CharacteristicTime = 138,
		Angle = 15,
		Stroke = 57,
		Distance = 131,
		Displacement = 2,
		Thickness = 51,
		Diameter = 52,
		CoolingRate = 48,
		ColumnNumber = 81,
		StressTriaxiality = 133,
		LodeStressParameter = 134,
		NormalizedMaxPrincipalStress = 135,
		MinorTrueStrain = 150,
	}

	public enum LogFormat {
		FromFileExtension = 0,
		XML = 520,
		SExpr = 518,
	}

	public enum Domain {
		Nil = 0,
		Friction = 16,
		MeshAdaptation = 41,
	}

	public enum SectionFormat {
		FromFileExtension = 0,
		STL = 503,
	}

	public enum LengthUnit {
		Auto = 0,
		m = 155,
		mm = 156,
		cm = 157,
		inch = 158,
	}

	public enum TrackingFieldsFormat {
		FromFileExtension = 0,
		XLS = 509,
	}

	public enum ValuesOnSheet {
		at_point = 2,
		at_time = 1,
		at_current_time = 3,
	}

	public enum MeshFormat {
		FromFileExtension = 0,
		CSV3D = 10,
		CSV2D = 9,
		DXF = 504,
		STL = 503,
		_3MF = 519,
		XLS = 509,
		XLSX = 510,
	}

	public enum FillMode {
		Gradient = 1,
		Discrete = 8,
		Isolines = 2,
		IsolinesMarks = 4,
	}

	public enum Colormap {
		Auto = 0,
		Jet = 1,
		Temp = 2,
		Grey = 3,
		Legacy = 4,
		Rainbow = 5,
		Turbo = 6,
		Viridis = 7,
		Plasma = 8,
		Copper = 9,
		RdYlBu = 10,
		Spectral = 11,
		Coolwarm = 12,
		Seismic = 13,
	}

	public enum HistogramBy {
		ByNodes = 0,
		ByElements = 1,
	}

	public enum Language {
		russian = 0,
		german = 1,
		spanish = 2,
		italian = 3,
		japanese = 4,
		thai = 5,
		chinese = 6,
		chinese_t = 7,
		polish = 8,
		portuguese = 9,
		turkish = 10,
		english = 11,
	}

	public enum DialogButton {
		Ok = 111,
		Cancel = 99,
		Yes = 121,
		No = 110,
		Apply = 97,
		Done = 100,
		Close = 122,
		Retry = 114,
		Skip = 107,
		Ignore = 105,
		Continue = 116,
		SaveAs = 115,
	}

	public enum Direction {
		X = 1,
		Y = 2,
		Z = 3,
	}

	public enum DisplayModes {
		visible = 1,
		show_mesh = 4,
		transparent = 16384,
		show_laps = 65536,
		show_geometric_mesh = 1024,
	}

	public enum OperationCreationMode {
		CreateAsNewProcess = 0,
		AddToCurrentChain = 1,
		InsertAfterParent = 2,
	}

	public enum OperationType {
		none = -1,
		electric = 7,
		deformation = 8,
		thermo = 9,
		extrusion = 10,
		ring_rolling = 11,
		wheel_rolling = 13,
		rolling = 14,
		cross_rolling = 15,
		sheet_bulk_forming = 16,
		reverse_rolling = 17,
		cyclic_heating = 18,
		extr_profile_cooling = 19,
	}

	public enum StopCondType {
		Distance = 1,
		Time = 2,
		ToolStroke = 3,
		ToolRotationAxis1 = 4,
		ToolRotationAxis2 = 5,
		MaxLoad = 6,
		FinalPosition = 7,
		FieldValue = 8,
		SolverTime = 9,
	}

	public enum SubRoutineType {
		lua = 0,
		wear = 1,
		strain_tensor = 2,
		stress_tensor = 3,
		stress_tensor_tool = 31,
		strain_tensor_tool = 32,
		press_wp = 4,
		press_tool = 15,
		distance_to_surface = 5,
		global_displacement = 6,
		strain_tensor_lagrangian = 12,
		fatigue = 21,
		debug_q = 1000,
		debug_tl = 1005,
		debug_qwp = 1010,
		debug_1 = 1001,
		debug_2 = 1002,
		FLDdiagram = 101,
		yield_tensor = 201,
		surface_normal = 1003,
		surface_normal_tool = 1007,
		gartfield = 1004,
		slippage = 3101,
		normal_velocity = 2002,
		track = 3001,
		forgingratio = 3005,
		track3 = 3003,
		outcrop = 3006,
		adaptation_tool = 52,
		adaptation_wp = 51,
		temperature = 61,
		ort = 3002,
		thickness = 55,
		cylindric = 10001,
		cyl_velocity = 10002,
		gtn_component = 10011,
		cylindric_tool = 10005,
		wp_damage = 20000,
		jmak = 20200,
		extr_streaking_lines = 20201,
		extr_tool_analysis = 21000,
		extr_fill_analysis = 21001,
		extr_overheating = 21002,
		extr_flow_time = 21003,
		extr_seam_quality = 21005,
		test_cubic = 30000,
		velocity_gradient = 22000,
		extr_skin = 21006,
	}

	public enum SystemOfUnits {
		metric = 0,
		english = 1,
		SI = 2,
	}

	public enum ViewOptions {
		ShowMinMax = 0,
		ShowCADColors = 1,
	}

	public enum DFProperty {
		constant = 1,
		table = 2,
		lua = 3,
	}

	public enum FitType {
		clearance = 1,
		interference = 2,
	}

	public enum AxisBlockType {
		p = 0,
		v = 1,
		x = 2,
		y = 3,
		z = 4,
	}

	public enum AxisInputMode {
		manual = 0,
		automatic = 1,
	}

	public enum POSITIONING_TYPE {
		bring_with_retract = 0,
		bring_without_retract = 1,
		none = 2,
		retract_to_contact = 3,
	}

	public enum AxisType {
		point = 0,
		direction = 1,
	}

	public enum HEAT_EXCHANGE {
		winkler = 1,
		joint = 2,
		const_temp = 3,
		none = 0,
	}

	public enum ClipType {
		inner = 0,
		outer = 1,
	}

	public enum FieldType {
		elem = 16,
		scalar = 1,
	}

	public enum ProtectionMode {
		no = 0,
		close_src = 1,
		encrypted = 2,
	}

	public enum Scope {
		public_scope = 0,
		private_scope = 1,
	}

	public enum ContourHierarchy {
		outer = 0,
		inner = 1,
	}

	public enum ContourClipping {
		trimming = 0,
		clipping = 1,
	}

	public enum BoxAxis {
		axis_z = 0,
		axis_y = 1,
		axis_x = 2,
	}

	public enum SolidBodyNodeValueCondition_ValueType {
		MIN_VALUE = 0,
		MAX_VALUE = 1,
	}

	public enum SolidBodyNodeValueCondition_ValueRegionInBody {
		ANY_NODE = 0,
		DEFINED_BODY_VOLUME_PERCENT = 1,
		DEFINED_VOLUME_OF_DEFORMATION_ZONE_PERCENT = 2,
	}

	public enum StopConditionSolverTimeType {
		PROCESS = 0,
		OPERATION = 1,
		BLOW = 2,
	}

	public enum SSTMode {
		nil = -1,
		source = 0,
		result = 3,
		preparation = 2,
		mesh_preparation = 1,
	}

	public enum TracingBoxType {
		line = 0,
		point = 1,
	}

	public enum Current {
		direct = 1,
		alternating = 2,
	}

	public enum ElectricPowerType {
		current = 1,
		voltage = 2,
		power = 3,
	}

	public enum SolveMethod {
		implicit_method = 0,
		explicit_method = 1,
	}

	public enum ThermoMethod {
		fe = 0,
		voronoi = 1,
	}

	public enum DEF_TYPE {
		none = -1,
		flat = 0,
		axis = 1,
		_3d = 2,
	}

	public enum MicrostructureModuleTypes {
		qform = 0,
		gmt = 1,
	}

	public enum ThermalProcessingType {
		heating = 1,
		tempering = 2,
		quenching = 3,
	}

	public enum MeshAdaptType {
		none = 0,
		adapt = 1,
		meshsize = 2,
	}

	public enum RRVStabilization {
		yes = 1,
		no = -1,
		automatic = 0,
	}

	public enum RRMRollVelocityType {
		angular = 0,
		linear = 1,
	}

	public enum RRFinDiam {
		max = 0,
		level = 1,
	}

	public enum RRStrategy {
		diam_height = 0,
		feed = 1,
		mandrell_roll = 2,
		mandrell_ring = 3,
	}

	public enum RRAxialVDispType {
		top = 0,
		sync = 1,
		ratio = 2,
	}

	public enum RRWheelStrat {
		rolls = 0,
		shaft = 1,
	}

	public enum RollingPusherVelocityType {
		automatic = 0,
		manual = 1,
	}

	public enum ExtrProcType {
		flow = 1,
		billet = 2,
		steady = 3,
	}

	public enum ExtrVelocityType {
		ram = 1,
		profile = 2,
	}

	public enum ExtrPrestageType {
		equal = 0,
		velocity = 1,
		time = 2,
	}

	public enum HardnessEstimationMethod {
		volume_fractions = 0,
		characteristic_time = 1,
	}

	public enum UltimateStrengthEstimationMethod {
		volume_fractions = 0,
		characteristic_time = 1,
	}

	public enum ToolJoinDeform {
		simple = 1,
		coupled = 2,
		stressed_state = 3,
	}

	public enum HeatTreatmentModuleTypes {
		qform = 0,
		gmt = 1,
	}

	public enum HardnessScales {
		HV = 1,
		HB = 2,
		HBW = 3,
		HRA = 4,
		HRB = 5,
		HRC = 6,
		HRD = 7,
		HRE = 8,
		HRF = 9,
		HRG = 10,
		HRH = 11,
		HRK = 12,
	}

	public enum ProfileDistortionTimeStepType {
		constant_time_step = 0,
		var_time_step_const_layer_thickness = 1,
	}

}
