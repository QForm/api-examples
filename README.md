# QForm API Samples

Samples of the QForm API are used to access the functionality of [QForm](https://qform3d.com) software in different programming languages and from other development tools. The API lets you automate the simulation, simplify the routine data preparation, work with any field's export values, and decrease reports preparation time.

Please, note that there are two licenses of API. The first one is about creating projects and automatization; the second one allows you to work with export data. You can either start QForm by API or run a script or an application from QForm.

QForm API supports the following programming languages and development tools:

- C#
- Python
- VB.net
- VBA
- MATLAB
- S-expr
- XML

Each folder in this repository contains the "Start" script just to describe how to link the API with each programming language. The more informative scripts show you how to work with export and data preparation functions. Pay attention to the **.md* files and read short descriptions of each example before you have started, otherwise you can get errors.

## Getting started

App Wizard represents a code redactor with basic functionality where you can input and invoke any function from the function tree to QForm, save/copy code. But QForm doesn't provide you an IDE, so for example, to use the functionality of Python, you should have Jupyter Notebook or Pycharm, etc. 

There is an *API* folder in the root directory of QForm. It contains programming libraries:

+ *QFormApiCom.exe* is used to install *.dll and *.tlb files to link Excel VBA with QForm API. 
+ *QFormApiNet.dll* is for *.net languages. It also allows you to connect to QForm using MATLAB.
+ QForm.cs is for C#.
+ *QFormAPI.py* is for Python.
+ To work with S-expr or XML, you do not need any libraries.
