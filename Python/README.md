Some scripts use additional packages for an interpreter. We work in Pycharm and Jupyter Notebook, but of course, you can use the IDE you want.

The *Start_example* script shows you how to connect to QForm using **Python**.

Here you can find the QFormAPI package: "C:\QForm\10.x.x\API\App\Python\QFormAPI.py".

The first step is to add the package to the interpreter:

```python
import sys
sys.path.append(r'C:\QForm\10.1.100.46\API\App\Python') #specify the path according to the QForm version
from QFormAPI import *
```

 Let's start QForm by API:

```python
qform = QForm()
qform.start(r'C:\QForm\10.1.100.46\x64') #specify the path according to the QForm version
```

You can find the *Start_example.py* in the repository and start QForm by script.
