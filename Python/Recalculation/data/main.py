'''

       Type:
              QForm software < qform3d.com > user script based on QForm API 10.2.0
       Description:
              This script allows you to start the simulation in QForm with special mode: if the simulation stops
              due to some reasons the operation will be recalculated automatically. The script changes the workpiece
              mesh adaptation factor and maximum strain increment, and starts the simulation from several previous steps.

              Invoke this script from QForm -- Applications. The operation must be calculated.
       Author:
              Yury Mylnikov < mylnikov@qform3d.com >
       Last change:
              June 30, 2022

'''

from sys import *
import QFormAPI
from QFormAPI import *
import tkinter as tk
from tkinter import *
import tkinter.messagebox
import random
import sys

qform = QForm()
state = True
state2 = True

def start():
    if len(txt1.get()) == 0:
        tkinter.messagebox.showwarning(title='Warn', message='Some fields are empty')
        return
    if len(txt2.get()) == 0:
        tkinter.messagebox.showwarning(title='Warn', message='Some fields are empty')
        return
    if len(txt3.get()) == 0:
        tkinter.messagebox.showwarning(title='Warn', message='Some fields are empty')
        return
    if len(txt4.get()) == 0:
        tkinter.messagebox.showwarning(title='Warn', message='Some fields are empty')
        return
    if len(txt5.get()) == 0:
        tkinter.messagebox.showwarning(title='Warn', message='Some fields are empty')
        return
    if len(txt6.get()) == 0:
        tkinter.messagebox.showwarning(title='Warn', message='Some fields are empty')
        return
    if len(txt7.get()) == 0:
        tkinter.messagebox.showwarning(title='Warn', message='Some fields are empty')
        return

    # Connect API to QForm project
    qform.start_or_attach(qform_path="", show_qform=True, no_console=True)

    ret1: Record = qform.record_get_last()  # Number of records in opened project
    j = 0 # global counter
    i = 0 # local counter
    count = int(txt7.get()) # Number of recalculations (local counter)
    record = []

    qform.start_simulation()

    txt8.insert(0, '0')
    txt9.insert(0, '0')

    global state2
    state2 = True

    # Cycle by global counter
    while (j < 5) and (state2 == True):
        root.update()
        txt9['state'] = 'normal'
        txt9.delete("0", END)
        txt9['state'] = 'disabled'
        record.append(qform.record_get_last().record)

        qform.start_simulation()
        root.update()
        record.append(qform.record_get_last().record)

        global state
        state = True
        txt9['state'] = 'normal'
        txt9.insert(0, j)
        txt9['state'] = 'disabled'

        # Cycle by local counter
        while (len(record) < count+2) and (state == True):
            root.update()
            arg1 = OptionalItemId()
            arg1.id = -1
            ret1: SimulationState = qform.state_operation(arg1)

            print(ret1.blow_progress)

            if ret1.blow_progress < 100:
                root.update()
                print(record[-1], '  -  ', record[-2])

                if record[-1] <= record[-2]:
                    root.update()
                    txt8['state'] = 'normal'
                    txt8.delete("0", END)
                    txt8['state'] = 'disabled'

                    # Calculating the number of steps backward
                    step = int(ret1.blow_record_count) - random.randint(inc_min.get(), inc_max.get())

                    # Calculating the strain increment
                    strain = Property()
                    strain.object_type = ObjectType.Operation
                    strain.object_id = -1
                    strain.path = r'max_strain_increment'
                    strain.property_type = PropertyType.Value
                    strain.value = r'{}'.format(round(random.uniform(float(max_def.get()), float(min_def.get())), 3))
                    qform.property_set(strain)

                    # Calculating the adaptation factor
                    adaptation = Property()
                    adaptation.object_type = ObjectType.Operation
                    adaptation.object_id = -1
                    adaptation.path = r'adapt_mult'
                    adaptation.property_type = PropertyType.Value
                    adaptation.value = r'{}'.format(round(random.uniform(float(max_adap.get()), float(min_adap.get())), 2))
                    qform.property_set(adaptation)

                    # Start simulation
                    arg2 = SimulationParams()
                    arg2.start_from_record = step
                    arg2.remesh_tools = False
                    arg2.stop_at_record = 0
                    arg2.max_records = 0
                    arg2.stop_at_process_time = 0
                    arg2.max_process_time = 0
                    arg2.max_calculation_time = 0
                    arg2.calculation_mode = CalculationMode.Chain
                    ret2: MainSimulationResult = qform.start_simulation_advanced(arg2)

                    record.append(qform.record_get_last().record)
                    txt8['state'] = 'normal'
                    txt8.insert(0, len(record)-2)
                    txt8['state'] = 'disabled'
                    root.update()

                else:
                    j = j + 1
                    root.update()
                    print('j = ', j)
                    record.clear()
                    state = False
                    break

            elif ret1.blow_progress == 100:
                state2 = False
                state = False
                arg3 = TraceMsg()
                arg3.msg = r'Simulation completed, 100%'
                arg3.type = MessageType.info
                qform.print(arg3)
                break

        else:
            break


# Сlear the values in the cells
def clear():
    txt1.delete("0", END)
    txt2.delete("0", END)
    txt3.delete("0", END)
    txt4.delete("0", END)
    txt5.delete("0", END)
    txt6.delete("0", END)
    txt7.delete("0", END)


# Close the app
def close():
    root.quit()

# Stop simulation
def stop():
    global state, state2
    state = False
    state2 = False

# Creating GUI
root = Tk()
root.geometry('400x350')
root.resizable(width=False, height=False)
root.title("QForm recalculation")
root["bg"] = 'gray95'

min_def = StringVar()
txt1 = Entry(root, textvariable=min_def, width=10)
txt1.place(x=245, y=40)
txt1.insert(0, '0.08')

max_def = StringVar()
txt2 = Entry(root, textvariable=max_def, width=10)
txt2.place(x=315, y=40)
txt2.insert(0, '0.12')

min_adap = StringVar()
txt3 = Entry(root, textvariable=min_adap, width=10)
txt3.place(x=245, y=70)
txt3.insert(0, '0.9')

max_adap = StringVar()
txt4 = Entry(root, textvariable=max_adap, width=10)
txt4.place(x=315, y=70)
txt4.insert(0, '1.2')

inc_min = IntVar()
txt5 = Entry(root, textvariable=inc_min, width=10)
txt5.place(x=245, y=100)
txt5.delete(0, END)
txt5.insert(0, '3')

inc_max = IntVar()
txt6 = Entry(root, textvariable=inc_max, width=10)
txt6.place(x=315, y=100)
txt6.delete(0, END)
txt6.insert(0, '6')

zap = IntVar()
txt7 = Entry(root, textvariable=zap, width=10)
txt7.place(x=245, y=130)
txt7.delete(0, END)
txt7.insert(0, '10')

zap1 = IntVar()
txt8 = Entry(root, textvariable=zap1, width=8, state=DISABLED)
txt8.place(x=110, y=290)
txt8.delete(0, END)

zap2 = IntVar()
txt9 = Entry(root, textvariable=zap2, width=8, state=DISABLED)
txt9.place(x=110, y=320)
txt9.delete(0, END)

lbl1 = Label(root, text="Maximum strain increment", font=('Barkentina', 10))
lbl1.place(x=5, y=40)

lbl2 = Label(root, text="Adaptation factor for workpiece", font=('Barkentina', 10))
lbl2.place(x=5, y=70)

lbl3 = Label(root, text="From", font=('Barkentina', 10))
lbl3.place(x=258, y=10)

lbl4 = Label(root, text="To", font=('Barkentina', 10))
lbl4.place(x=335, y=10)

lbl5 = Label(root, text="Number of records backward", font=('Barkentina', 10))
lbl5.place(x=5, y=100)

lbl6 = Label(root, text="Number of attempts", font=('Barkentina', 10))
lbl6.place(x=5, y=130)

lbl7 = Label(root, text="Local counter", font=('Barkentina', 10))
lbl7.place(x=5, y=290)

lbl8 = Label(root, text="Global counter", font=('Barkentina', 10))
lbl8.place(x=5, y=320)

btn1 = Button(root, text="START", command=start, bg='#00ff64', fg='black', font=8)
btn1.place(x=246, y=160, height=25, width=132)

btn2 = Button(root, text="Clear", command=clear, font=('Barkentina', 10))
btn2.place(x=110, y=240, height=25, width=80)

btn3 = Button(root, text="Exit", command=close, font=('Barkentina', 10))
btn3.place(x=200, y=240, height=25, width=80)

btn4 = Button(root, text="STOP", command=stop, bg='#FF0000', fg='black', font=6)
btn4.place(x=246, y=190, height=25, width=132)

root.mainloop()
