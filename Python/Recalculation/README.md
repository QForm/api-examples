## QForm Recalculation v.3

You can find either the *exe application or the *py script of the program in the data folder. Start working with the app using the attached *qform file. But of course, it is possible to stick to your own project.   

This application allows you to start the simulation in QForm with special mode: if the simulation stops due to some reasons, the operation will be recalculated automatically. The app changes the workpiece mesh adaptation factor and maximum strain increment, and starts the simulation from several previous steps.

The application could be helpful for simulation of projects with very complex geometry and remeshing processes: with laps, mesh dividing, complex contact problems in simulations with elastic tools or several workpieces. 

### How to run the application?

1. Open a QForm project you would like to simulate
2. Jump to the *Applications* on the main menu tab and press *Run Application*. Select *Recalculation.exe* file.
3. Specify the recalculation parameters in the opened window
4. Click *Start*
5. If you want to stop the simulation after current recalculation - press *Stop*

### How does it work?

Here is the application window:

![](data/QForm_recalculation.png)

Following parameters must be set:

\-     **Maximum strain increment** 

Before recalculation starts, the *Maximum strain increment* in the simulation parameters of current operation will be randomly changed within a given range (From … To)

\-     **Adaptation factor for workpiece** 

Before recalculation starts, the *Adaptation factor for workpiece mesh* in the simulation parameters of current operation will be randomly changed within a given range (From ... To)

\-     **Number of records backward**

Before recalculation starts, the simulation record will be automatically decreased due to the randomly specified *Number of records backward* within a given range (From ... To)

\-     **Number of attempts** 

Max. number of local recalculations. For example, if *Number of attempts* equal to 10, and we have and error on the 40th step and after 10 recalculations we are still on the 40th step or less - simulation will be stopped.

Counters:

of records backward* within a given range (From ... To)

\-     **Local counter** shows us current number of recalculations if the simulation is not continued against initial step

\-     **Global counter** shows us total number of recalculations
